//
//  MazaratFilterView.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import YNDropDownMenu
import McPicker
import ActionSheetPicker_3_0

class MazaratFilterView: YNDropDownView {
    
    var delegateTemp: MazaratPopUpDelegte?
    private var typeTemp = "-1"
    private var dateTemp = "-1"
    private var CityTemp = "-1"
    private var AgentTemp = "-1"
    
    
    @IBOutlet weak var dateView: UIView!{
        didSet{
            initViews()
        }
    }
    @IBOutlet weak var dateLbl: UITextField!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLbl: UITextField!
    
    @IBOutlet weak var EALbl: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let date = Date()
        dateTemp = getDate(date: date)
        //        self.initViews()
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        var EATemp = "-1"
        if EALbl.text != ""{
            EATemp = EALbl.text ?? "-1"
        }
        
        delegateTemp?.popUpValues(TripDate: dateTemp, EA_CODE: EATemp, CityID: CityTemp, PageNumber: "1", country: CityTemp)
        self.hideMenu()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegateTemp?.justBeingCancelled()
        self.hideMenu()
        
    }
    
    
    func initViews() {
        Countries.sharedInstance.getCountriesServer()
        
        
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        cityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        
    }
    
    
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in
            
            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self)
        
    }
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [Cities.sharedInstance.getCitiesNameArr(lang: false)])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0] {
                self.cityLbl.text = StringTemp
                self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
            }
        }
    }
    
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
         return "\(day)-\(month)-\(year)"
    }
    
    
}
