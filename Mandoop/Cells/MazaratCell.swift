//
//  MazaratCell.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit

class MazaratCell: UITableViewCell {
    
    
    @IBOutlet weak var date1: MyLabel!
    @IBOutlet weak var headerView: UIView!
    
    
    @IBOutlet weak var flightImg: UIImageView!
    
    @IBOutlet weak var mazarLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var ReqId: UILabel!
    @IBOutlet weak var ReqIdLbl: UILabel!
    @IBOutlet weak var NOMUT: UILabel!
    @IBOutlet weak var NoMutLbl: UILabel!
    @IBOutlet weak var EA: UILabel!
    @IBOutlet weak var EALbl: UILabel!
    
    
    @IBOutlet weak var EACountry: UILabel!
    @IBOutlet weak var EACountryLbl: UILabel!
    @IBOutlet weak var Hotel: UILabel!
    @IBOutlet weak var HotelLbl: UILabel!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setuplang()
    }
    
    func UISetup(arrival:Arrival){
        if Language.sharedInstance.getLang(){
            HotelLbl.text = arrival.FROM_AR
            mazarLbl.text = arrival.TO_AR
            EACountryLbl.text = arrival.NAT_NAME
        }else{
            HotelLbl.text = arrival.FROM_LA
            mazarLbl.text = arrival.TO_LA
            EACountryLbl.text = arrival.NAT_NAME_LA
        }
        
        ReqIdLbl.text = arrival.SER_REQ
        NoMutLbl.text = "\(String(describing: arrival.ARN_TOTAL_COUNT ?? -1))"
        EALbl.text = "\(arrival.EA_CODE ?? -1)"
        timeLbl.text = arrival.TRIP_DATE
    }
    
    private func setuplang(){
        date1.text = NSLocalizedString("Date :", comment: "رقم الطلب :")
        ReqId.text = NSLocalizedString("Request Id :", comment: "رقم الطلب :")
        NOMUT.text = NSLocalizedString("No Mutamera :", comment: "عدد المعتمرين :")
        EA.text = NSLocalizedString("EA :", comment: "الوكيل :")
        
        Hotel.text = NSLocalizedString("Hotel :", comment: "الفندق :")
        EACountry.text = NSLocalizedString("EA Country :", comment: "الجنسية :")
        
        
        
    }
    
    
}
