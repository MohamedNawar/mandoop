//
//  ZBDropDownMenu.swift
//  zigbang_ios
//
//  Created by YiSeungyoun on 2017. 2. 13..
//  Copyright © 2017년 chbreeze. All rights reserved.
//

import UIKit
import YNDropDownMenu
import McPicker
import ActionSheetPicker_3_0



class ZBFilterFeatureView: YNDropDownView {
    
    var delegateTemp: ArrivalsPopUpDelegte?
    private var typeTemp = "-1"
    private var dateTemp = "-1"
    private var CityTemp = "-1"
    private var AgentTemp = "-1"
    
    @IBOutlet weak var flightTypeView: UIView!{
        didSet{
            initViews()
        }
    }
    @IBOutlet weak var flightTypeLbl: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLbl: UITextField!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLbl: UITextField!
    
    @IBOutlet weak var agentView: UIView!
    @IBOutlet weak var agentLbl: UITextField!
    
    
    @IBOutlet weak var EALbl: UITextField!
    
    @IBOutlet weak var flightNumLbl: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let date = Date()
        dateTemp = getDate(date: date)
//        self.initViews()
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        var EATemp = "-1"
        var flightNumTemp = "-1"
        if EALbl.text != ""{
            EATemp = EALbl.text ?? "-1"
        }
        if flightNumLbl.text != ""{
            flightNumTemp = flightNumLbl.text ?? "-1"
        }
        delegateTemp?.popUpValues(flightType: typeTemp, EA_CODE: EATemp , TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AIRLINE_NO: flightNumTemp, PageNumber: "1", country: "")
        self.hideMenu()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegateTemp?.justBeingCancelled()
        self.hideMenu()
        
    }
    
    
    func initViews() {
        Countries.sharedInstance.getCountriesServer()
        Agents.sharedInstance.getAgentsServer()
        flightTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightTypePressed(_:))))
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        cityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        agentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.agentPressed(_:))))
    }
    
    @objc private func flightTypePressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [["Arrival","Departure","All"]])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
            self.flightTypeLbl.text = StringTemp
            switch StringTemp{
            case "Arrival":
                self.typeTemp = "1"
            case "وصول":
                self.typeTemp = "1"
            case "Departure":
                self.typeTemp = "2"
            case "سفر":
                self.typeTemp = "2"
            default:
                self.typeTemp = "-1"
            }
        }
        }
    }
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in

            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self)
        
    }
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [Cities.sharedInstance.getCitiesNameArr(lang: false)])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0] {
                self.cityLbl.text = StringTemp
                self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
            }
        }
    }
    
    @objc private func agentPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [Agents.sharedInstance.getAgentsNameArr(lang: false)])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            
            if let StringTemp = selections[0] {
                self.agentLbl.text = StringTemp
                self.AgentTemp = Agents.sharedInstance.getAgentsCommercialRegNo(name: StringTemp)
            }
            
            
        }
    }
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        return "\(day)-\(month)-\(year)"
    }


}

