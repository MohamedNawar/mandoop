//
//  MazaratCell.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import MOLH
class PlaceCell: UITableViewCell {
    
    
    @IBOutlet weak var headerView: UIView!
    
    
    @IBOutlet weak var flightImg: UIImageView!

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var ReqId: UILabel!
    @IBOutlet weak var ReqIdLbl: UILabel!
    @IBOutlet weak var NOMUT: UILabel!
    @IBOutlet weak var NoMutLbl: UILabel!
    @IBOutlet weak var EA: UILabel!
    @IBOutlet weak var EALbl: UILabel!
    
    @IBOutlet weak var hotelLbl: UILabel!
    @IBOutlet weak var EACountry: UILabel!
    @IBOutlet weak var EACountryLbl: UILabel!
    @IBOutlet weak var Reaminig: UILabel!
    @IBOutlet weak var RemainingLbl: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setuplang()
    }
    
    func UISetup(arrival:Arrival){
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            if arrival.TRANS_TYPE == "ARRIVAL" {
                self.hotelLbl.text = arrival.TO_AR
            }else if arrival.TRANS_TYPE == "CHECK-IN" {
                self.hotelLbl.text = arrival.TO_AR
            }else{
                self.hotelLbl.text = arrival.FROM_AR
            }
            EACountryLbl.text = arrival.NAT_NAME
        }else{
            if arrival.TRANS_TYPE == "ARRIVAL" {
                self.hotelLbl.text = arrival.TO_LA
            }else if arrival.TRANS_TYPE == "CHECK-IN" {
                self.hotelLbl.text = arrival.TO_LA
            }else{
                self.hotelLbl.text = arrival.FROM_LA
            }
            EACountryLbl.text = arrival.NAT_NAME_LA

        }
        
        timeLbl.text = arrival.TRIP_DATE
        ReqIdLbl.text = arrival.SER_REQ
        NoMutLbl.text = "\(String(describing: arrival.ARN_TOTAL_COUNT ?? -1))"
        EALbl.text = "\(arrival.EA_CODE ?? -1)"
        RemainingLbl.text = arrival.REMINING
        
    }
    
    private func setuplang(){
        
        time.text = NSLocalizedString("Time :", comment: "الوقت :")
        ReqId.text = NSLocalizedString("Request Id :", comment: "رقم الطلب :")
        NOMUT.text = NSLocalizedString("No Mutamera :", comment: "عدد المعتمرين :")
        EA.text = NSLocalizedString("EA :", comment: "الوكيل :")

        EACountry.text = NSLocalizedString("EA Country :", comment: "الجنسية :")
        Reaminig.text = NSLocalizedString("Check In :", comment: "دخول/خروج :")
        
        
        
    }

    
}
