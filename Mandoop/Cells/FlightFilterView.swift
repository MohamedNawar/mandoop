//
//  FlightFilterView.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//


import UIKit
import YNDropDownMenu
import McPicker
import ActionSheetPicker_3_0



class FlightFilterView: YNDropDownView {
    
    var delegateTemp: FlightPopUpDelegte?
    private var typeTemp = "-1"
    private var dateTemp = "-1"
    private var CityTemp = "-1"
    private var CountryTemp = "-1"
    private var statusTemp = "-1"
    
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            self.scrollView.contentSize.height = self.scrollView.contentSize.height + 100
        }
    }
    @IBOutlet weak var flightTypeView: UIView!{
        didSet{
            initViews()
        }
    }
    @IBOutlet weak var flightTypeLbl: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLbl: UITextField!
    
    @IBOutlet weak var CountryView: UIView!
    @IBOutlet weak var CountryLbl: UITextField!
    
    @IBOutlet weak var CityView: UIView!
    @IBOutlet weak var CityLbl: UITextField!
    
    
    
    @IBOutlet weak var airlineCodeView: UIView!
    
    @IBOutlet weak var airlineCodeLbl: UITextField!
    @IBOutlet weak var StatusView: UIView!
    @IBOutlet weak var StatusLbl: UITextField!
    @IBOutlet weak var flightNumLbl: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        
        self.initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let date = Date()
        dateTemp = getDate(date: date)
        //        self.initViews()
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        var AirlineCodeTemp = "-1"
        var flightNumTemp = "-1"
        
        
        if airlineCodeLbl.text != ""{
            AirlineCodeTemp = airlineCodeLbl.text ?? "-1"
        }
        if flightNumLbl.text != ""{
            flightNumTemp = flightNumLbl.text ?? "-1"
        }
        
        
        delegateTemp?.popUpValues(flightType: typeTemp, AirlineCode: AirlineCodeTemp , TripDate: dateTemp, FlightNO: flightNumTemp, TripStatus: statusTemp, TripCountry: CountryTemp, TripCity: CityTemp, PageNumber: "1")
        self.hideMenu()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegateTemp?.justBeingCancelled()
        self.hideMenu()
        
    }
    
    
    func initViews() {
        Countries.sharedInstance.getCountriesServer()
//        Agents.sharedInstance.getAgentsServer()
        flightTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightTypePressed(_:))))
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        CityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        CountryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.countryPressed(_:))))
        StatusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.statusPressed(_:))))
        
    }
    
    @objc private func flightTypePressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [["Arrival","Departure","All"]])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
                self.flightTypeLbl.text = StringTemp
                switch StringTemp{
                case "Arrival":
                    self.typeTemp = "1"
                case "وصول":
                    self.typeTemp = "1"
                case "Departure":
                    self.typeTemp = "2"
                case "سفر":
                    self.typeTemp = "2"
                default:
                    self.typeTemp = "-1"
                }
            }
        }
    }
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in
            
            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self)
        
    }
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [Cities.sharedInstance.getCitiesNameArr(lang: false)])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0] {
                self.CityLbl.text = StringTemp
                self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
            }
        }
    }
    
    @objc private func countryPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [Countries.sharedInstance.getCountriesNameArr(lang: false)])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0] {
                self.CountryLbl.text = StringTemp
                let id = Countries.sharedInstance.getCountriesId(name: StringTemp)
                self.CountryTemp = id
                Cities.sharedInstance.getCitiesServer(id: Int(id) ?? -1)
            }
        }
    }
    
    @objc private func statusPressed(_ sender: UITapGestureRecognizer) {
        let mcPicker = McPicker(data: [["Active","Not Active"]])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            
            if let StringTemp = selections[0] {
                self.StatusLbl.text = StringTemp
                if StringTemp == "Active"{
                    self.statusTemp = "0"
                }else{
                    self.statusTemp = "512"
                }
            }
        }
    }
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
         return "\(day)-\(month)-\(year)"
    }
    
    
}

