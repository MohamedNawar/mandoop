//
//  flieghtCell.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/9/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import MOLH
class flieghtCell: UITableViewCell {
    
    @IBOutlet weak var flightMainNum: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var flightNum: UILabel!
    @IBOutlet weak var flightImg: UIImageView!
    @IBOutlet weak var AirLine: UILabel!
    @IBOutlet weak var AirLineLbl: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var ReqId: UILabel!
    @IBOutlet weak var ReqIdLbl: UILabel!
    @IBOutlet weak var NOMUT: UILabel!
    @IBOutlet weak var NoMutLbl: UILabel!
    @IBOutlet weak var EA: UILabel!
    @IBOutlet weak var EALbl: UILabel!
    @IBOutlet weak var hotel: UILabel!
    @IBOutlet weak var hotelLbl: UILabel!
    @IBOutlet weak var EACountry: UILabel!
    @IBOutlet weak var EACountryLbl: UILabel!
    @IBOutlet weak var Reaminig: UILabel!
    @IBOutlet weak var RemainingLbl: UILabel!
    @IBOutlet weak var Trans: UILabel!
    @IBOutlet weak var transLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setuplang()
    }
    
    func UISetup(arrival:Arrival){
        
        if Language.sharedInstance.getLang(){
            if arrival.TRANS_TYPE == "ARRIVAL" {
                self.hotelLbl.text = arrival.TO_AR
                flightImg.image = #imageLiteral(resourceName: "flyIn")
            }else{
                self.hotelLbl.text = arrival.FROM_AR
                flightImg.image = #imageLiteral(resourceName: "flyOut")
            }
        }else{
            if arrival.TRANS_TYPE == "ARRIVAL" {
                self.hotelLbl.text = arrival.TO_LA
                flightImg.image = #imageLiteral(resourceName: "flyIn")
            }else{
                self.hotelLbl.text = arrival.FROM_LA
                flightImg.image = #imageLiteral(resourceName: "flyOut")
            }
        }
        if MOLHLanguage.currentAppleLanguage() == "en" {
        self.flightNum.text = "\(String(describing: arrival.AIRLINE_NO ?? -1))"
        self.AirLineLbl.text = arrival.AIRLINE_LA
        self.timeLbl.text = arrival.TRIP_DATE
        self.ReqIdLbl.text = arrival.SER_REQ
        self.NoMutLbl.text = "\(String(describing: arrival.ARN_TOTAL_COUNT ?? -1))"
        self.EALbl.text = "\(arrival.EA_NAME_LA ?? "")"
        self.EACountryLbl.text = arrival.NAT_NAME_LA
        self.RemainingLbl.text = arrival.REMINING
        self.transLbl.text = arrival.LCT_NAME_LA
        }else{
            self.flightNum.text = "\(String(describing: arrival.AIRLINE_NO ?? -1))"
            self.AirLineLbl.text = arrival.AIRLINE
            self.timeLbl.text = arrival.TRIP_DATE
            self.ReqIdLbl.text = arrival.SER_REQ
            self.NoMutLbl.text = "\(String(describing: arrival.ARN_TOTAL_COUNT ?? -1))"
            self.EALbl.text = "\(arrival.EA_NAME ?? "")"
            self.EACountryLbl.text = arrival.NAT_NAME
            self.RemainingLbl.text = arrival.REMINING
            self.transLbl.text = arrival.LCT_NAME_AR
        }
    }
    
    
    private func setuplang(){
        flightMainNum.text = NSLocalizedString("Flight No.", comment: "رقم الرحلة")
        AirLine.text = NSLocalizedString("Airlines :", comment: "خطوط الطيران :")
        time.text = NSLocalizedString("Time :", comment: "الوقت :")
        ReqId.text = NSLocalizedString("Request Id :", comment: "رقم الطلب :")
        NOMUT.text = NSLocalizedString("No Mutamera :", comment: "عدد المعتمرين :")
        EA.text = NSLocalizedString("EA :", comment: "الوكيل :")
        
        hotel.text = NSLocalizedString("Hotel :", comment: "الفندق :")
       EACountry.text = NSLocalizedString("EA Country :", comment: "الجنسية :")
        Reaminig.text = NSLocalizedString("Remaining :", comment: "الوقت المتبقي :")
        Trans.text = NSLocalizedString("Transport Company :", comment: "شركة النقل :")
        
        
    }

    
    
}
