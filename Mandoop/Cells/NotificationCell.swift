//
//  NotificationCell.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/12/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var flightName: UILabel!
    @IBOutlet weak var flightImg: UIImageView!
    
//    @IBOutlet weak var flightNum: UILabel!
//
//    @IBOutlet weak var Year: UILabel!
    @IBOutlet weak var dayAndMonth: UILabel!
    
    @IBOutlet weak var squredView: UIView!
    
    @IBOutlet weak var lineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        lineView.clipsToBounds = true
//        lineView.layer.cornerRadius = 10
//        lineView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }

    func UISetup(notify:Notification){
        let time = notify.NOTIFICATION_TIME ?? ""
        self.flightName.text = notify.NOTIFICATION_MESSAGE
        
//        self.Year.text = notify.NOTIFICATION_TIME
        
        
        self.dayAndMonth.text = time.toDate(format: "yyyy-MM-dd'T'HH:mm:ss")?.asString()
        
        flightImg.image = #imageLiteral(resourceName: "flyIn")
        
//        if arrival.TRANS_TYPE == "ARRIVAL" {
//            self.flightName.text = notify.
//            
//        }else{
//            self.flightName.text = "رحلة سفر معتمرين"
//            flightImg.image = #imageLiteral(resourceName: "flyOut")
//        }
    }
    
}
