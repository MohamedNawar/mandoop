//
//  FlightCell2.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import MOLH
class FlightCell2: UITableViewCell {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var flightMainNum: UILabel!
    @IBOutlet weak var flightNum: UILabel!
    @IBOutlet weak var flightImg: UIImageView!
    @IBOutlet weak var AirLine: UILabel!
    @IBOutlet weak var AirLineLbl: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var ReqId: UILabel!
    @IBOutlet weak var ReqIdLbl: UILabel!
    @IBOutlet weak var NOMUT: UILabel!
    @IBOutlet weak var NoMutLbl: UILabel!
    @IBOutlet weak var EA: UILabel!
    @IBOutlet weak var EALbl: UILabel!
    @IBOutlet weak var hotel: UILabel!
    @IBOutlet weak var hotelLbl: UILabel!
    @IBOutlet weak var EACountry: UILabel!
    @IBOutlet weak var EACountryLbl: UILabel!
    @IBOutlet weak var Trans: UILabel!
    @IBOutlet weak var transLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setuplang()
    }
    
    func UISetup(arrival:Flight){
        if MOLHLanguage.currentAppleLanguage() == "en" {
        self.flightNum.text = arrival.FLIGHT_NO
        self.AirLineLbl.text = arrival.AT_NAME_LA
        self.timeLbl.text = "\(arrival.DATE_TRIP?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asTimeString() ?? "")"
        self.ReqIdLbl.text = "\(arrival.DATE_TRIP?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asString() ?? "")"
        self.NoMutLbl.text = arrival.CNTRY_NAME_LA
        self.EALbl.text = arrival.ORIGIN_CITY_LA
        self.hotelLbl.text = arrival.DESTINATION_CITY_LA
        self.EACountryLbl.text = arrival.PORT_LA
        self.transLbl.text = arrival.TERMINAL_LA
        }else{
            self.flightNum.text = arrival.FLIGHT_NO
            self.AirLineLbl.text = arrival.AT_NAME_AR
            self.timeLbl.text = "\(arrival.DATE_TRIP?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asTimeString() ?? "")"
            self.ReqIdLbl.text = "\(arrival.DATE_TRIP?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asString() ?? "")"
            self.NoMutLbl.text = arrival.CNTRY_NAME_AR
            self.EALbl.text = arrival.ORIGIN_CITY
            self.hotelLbl.text = arrival.DESTINATION_CITY
            self.EACountryLbl.text = arrival.PORT
            self.transLbl.text = arrival.TERMINAL
        }
//        
//        if arrival.TRANS_TYPE == "ARRIVAL" {
//            self.hotelLbl.text = arrival.TO_LA
//            flightImg.image = #imageLiteral(resourceName: "flyIn")
//        }else{
//            self.hotelLbl.text = arrival.FROM_LA
//            flightImg.image = #imageLiteral(resourceName: "flyOut")
//        }
    }
    
    private func setuplang(){
        flightMainNum.text = NSLocalizedString("Flight No.", comment: "رقم الرحلة")
        AirLine.text = NSLocalizedString("Airlines :", comment: "خطوط الطيران :")
        time.text = NSLocalizedString("Time :", comment: "الوقت :")
        ReqId.text = NSLocalizedString("Date :", comment: "التاريخ :")
        NOMUT.text = NSLocalizedString("From Country :", comment: "من دولة :")
        EA.text = NSLocalizedString("From city :", comment: "من مدينة :")
        
        hotel.text = NSLocalizedString("To City (Destintion) :", comment: "إلي مدينة :")
        EACountry.text = NSLocalizedString("Port :", comment: "المنفذ :")
        
        Trans.text = NSLocalizedString("Terminal :", comment: "الصالة :")
        
        
    }

    
    
    
}
