//
//  Mo3tamereenCell.swift
//  Mandoop
//
//  Created by Moaz Ezz on 8/18/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit

class Mo3tamereenCell: UITableViewCell {
    
    
    
    var selectPressed : ((Bool)->())?
    var selectVar = false{
        didSet{
            checkBox.isSelected = selectVar
            selectPressed?(selectVar)
        }
    }
    
    
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var passport: UILabel!
    @IBOutlet weak var passportLbl: UILabel!
    
    
    @IBOutlet weak var VisaNo: UILabel!
    
    @IBOutlet weak var VisaNoLbl: UILabel!
    
    
    @IBOutlet weak var Nat: UILabel!
    
    
    @IBOutlet weak var NatLbl: UILabel!
    
    @IBOutlet weak var Arrival: UILabel!
    
    @IBOutlet weak var arrivalLbl: UILabel!
    
    @IBOutlet weak var departure: UILabel!
    
    @IBOutlet weak var departureLbl: UILabel!
    
    @IBOutlet weak var checkBox: UIButton!
    
    
    func SetupUI(item: Mo3tameer, isSelectable: Bool) {
        self.name.text = item.M_FULL_NAME
        self.passportLbl.text = item.M_PASSPORT_NO
        self.VisaNoLbl.text = item.VISA_NO
        self.NatLbl.text = Language.sharedInstance.getLang() ? item.NT_NAME_AR : item.NT_NAME_LA
        self.arrivalLbl.text = item.EE_ENTRY_DATE?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asString()
        self.departureLbl.text = item.EE_EXIT_DATE?.toDate(format: "YYYY-MM-dd'T'HH:mm:ss")?.asString()
        selectVar = item.isSelected ?? false
        checkBox.isHidden = isSelectable
        
    }
    
    @IBAction func checkBoxPressed(_ sender: Any) {
        selectVar = !selectVar
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        headerView.clipsToBounds = true
        headerView.layer.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setuplang()
        checkBox.layer.borderColor = UIColor.darkGray.cgColor
        checkBox.layer.borderWidth = 1.0
        checkBox.layer.cornerRadius = 2
    }

    private func setuplang(){
        passport.text = NSLocalizedString("Passport No :", comment: "رقم الجواز :")
        VisaNo.text = NSLocalizedString("Visa No :", comment: "رقم الفيزا :")
        Nat.text = NSLocalizedString("Nat :", comment: "الجنسية :")
        Arrival.text = NSLocalizedString("Arrival :", comment: "الوصول :")
        departure.text = NSLocalizedString("Departure :", comment: "المغاردة :")

    }
    
    
    
}
