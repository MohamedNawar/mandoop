//
//  Cities.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/11/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation
import Alamofire
import PKHUD
import MOLH
class Cities {
    static let sharedInstance = Cities()
    private init() {}
    
    private var cities = [City]()
    
    public func getCities() -> [City]{
        return cities
    }
    public func getCitiesNameArr(lang : Bool) -> [String]{
        
        var temp = [String]()
        if lang{//true = Arabic
            for i in cities{
                temp.append(i.CityAr ?? "")
            }
        }else{//false = English
            for i in cities{
                temp.append(i.CityLa ?? "")
            }
        }
        return temp
        
    }
    
    public func getCitiesId(name : String) -> String{
        for i in cities{
            if i.CityAr == name{
                return "\(i.CityId ?? -1)"
            }
            if i.CityLa == name{
                return "\(i.CityId ?? -1)"
            }
        }
        return "-1"
    }
    
    public func getCityName(id : Int,lang : Bool) -> String{
        for i in cities{
            if i.CityId == id{
                if lang {
                    return "\(i.CityAr ?? "")"
                }else{
                    return "\(i.CityLa ?? "")"
                }
                
            }
        }
        return ""
    }
    
    
    public func getCitiesServer(id:Int) {

        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        
        HUD.show(.progress)
        Alamofire.request(APIs.sharedInstance.Cities(id: id), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print("error from the server")
                   
                }else{
                    
                    do {
                        self.cities = try JSONDecoder().decode([City].self, from: response.data!)
                        
 
                    }catch{
                        print("error in parsing the data")
                        
                    }
                }
                HUD.hide()
            case .failure(_):
                print("حدث خطأ برجاء إعادة المحاولة لاحقاً")
                HUD.hide()
                break
            }
        }
        
        
    }
    
    
    public func getCitiesForUser(){
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        
        HUD.show(.progress)
        Alamofire.request(APIs.sharedInstance.CitiesForUser(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print("error from the server")
                    
                }else{
                    
                    do {
                        self.cities = try JSONDecoder().decode([City].self, from: response.data!)
                        
                        
                        
                    }catch{
                        print("error in parsing the data")
                        
                    }
                }
                HUD.hide()
            case .failure(_):
                print("حدث خطأ برجاء إعادة المحاولة لاحقاً")
                HUD.hide()
                break
            }
        }
        
        
    }
    
}

class City : Decodable {

    var CountryId : Int?
    var CityId : Int?
    var CityAr : String?
    var CityLa : String?
    var Status : Int?
    
}
