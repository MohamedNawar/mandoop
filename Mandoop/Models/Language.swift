//
//  Language.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/19/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import MOLH

class Language {
    static let sharedInstance = Language()
    private init() {}
    func getLang() -> Bool {
        if MOLHLanguage.currentAppleLanguage() == "en"{
            return false

        }else{
            return true

        }
        return true
    }

}
