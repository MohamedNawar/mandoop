//
//  Representative.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation

class Representative : Decodable{

    var URP_ID : Int?
    var URP_NAME : String?
    var URP_MOBILE : Int?
    var URP_CITY_CODE : Int?
    var CT_NAME_AR : String?
    var CT_NAME_LA : String?
    var G_NAME_LA : String?
    var G_NAME_AR : String?
    var MOB_PASSWORD : String?
    var PUSH_NOTIFICATION : Int?
    var URP_IDENTIFIER_ID : Int?
    var ONE_SIGNAL_ID : String?
 
    
}
