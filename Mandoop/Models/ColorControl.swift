//
//  ColorControl.swift
//  NewArt
//
//  Created by Moaz Ezz on 5/9/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//



import Foundation
import UIKit


//    MainColor = UIColor(hex: "ff8181")
//    MainDarkColor =  UIColor(hex: "a81382")



class ColorControl {
    static let sharedInstance = ColorControl()
    private init() {}
    
    private let MainColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
    private let SecondColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    private let AlertColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    
    
    
    public func getMainColor() -> UIColor {
        return MainColor
    }
    
    public func getSecoundColor() -> UIColor {
        return SecondColor
    }
    
    public func getAlertColor() -> UIColor {
        return AlertColor
    }
    
    
    public func getNormalFont(size: CGFloat) -> UIFont? {
        let temp = UIFont(name: "Cairo", size: size)
        return temp
    }
    
    public func getBoldFont(size: CGFloat) -> UIFont? {
        let temp = UIFont(name: "Cairo-Bold", size: size)
        return temp
    }
    
    
    
    
    
}





