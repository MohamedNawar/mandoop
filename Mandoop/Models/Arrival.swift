//
//  Arrival.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/10/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation


class Arrival : Decodable , Comparable {
    static func < (lhs: Arrival, rhs: Arrival) -> Bool {
       return false
    }
    
    static func == (lhs: Arrival, rhs: Arrival) -> Bool {
        return true
    }
    
    var AIRLINE : String?
    var AIRLINE_NO : Int?
    var TRANS_TYPE : String?
    var REMINING : String?
    var EA_CODE : Int?
    var EA_NAME : String?
    var UO_CODE : Int?
    var SER_REQ : String?
    var NAT_NAME : String?
    var ARN_TOTAL_COUNT : Int?
    var TRIP_DATE : String?
    var FROM_AR : String?
    var FROM_LA : String?
    var TO_AR : String?
    var TO_LA : String?
    var TRS_OPERATION_ID : Int?
    var BUS_DATE : String?
    var ARN_SUPERVISOR : String?
    var ARN_SUPERVISOR_MOB : String?
    var LCT_NAME_AR : String?
    var ARN_ID : Int?
    var AIRLINE_LA : String?
    var EA_NAME_LA : String?
    var NAT_NAME_LA : String?
    var LCT_NAME_LA : String?

}
//
//{
//    Address = "<null>";
//    AgentAr = "\U0627\U0644\U0648\U0627\U062d\U0629 \U0627\U0644\U0632\U0631\U0642\U0627\U0621";
//    AgentCode = 29745;
//    AgentLa = "PALMERAIE BLUE";
//    Capital = 0;
//    CommercialRegNo = "17D/7";
//    CountryId = 212;
//    Email = "palmeraie.bleu@gmail.com";
//    Fax = 00212522387560;
//    FreePhone = "<null>";
//    Iata = "542 12594";
//    NoOwners = 1;
//    Phone = 00212522387555;
//    PoBox = "<null>";
//    ResponsibleMobile = "<null>";
//    ResponsibleName = "<null>";
//    ResponsiblePassport = "<null>";
//    Status = 0;
//    Url = "<null>";
//}
//
//
//
//
