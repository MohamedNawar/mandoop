//
//  APIs.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/9/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation

class APIs {
    static let sharedInstance = APIs()
    private init() {}
      let url = "http://87.101.211.201/mobileapi/"
   //  let url = "http://87.101.211.201/mobileapi/"
//    let url = "http://service.babalumra.com/MobileAPI"
//    let url = "http://service.babalumra.com/MobileAPI/"
    let pageSize = "1"
    
    public func LogIn() -> String{
        return url + "token"
    }
    
    public func getArrivalAndDeparture(flightType:String,EA_CODE:String,TripDate:String,CityID:String,ReqID:String,AirlineComp:String,AIRLINE_NO:String,PageNumber:String) -> String{
        print( url + "api/Notifications/GetArrivalDepartureNotif?flightType=\(flightType)&EA_CODE=\(EA_CODE)&TripDate=\(TripDate)&CityID=\(CityID)&ReqID=\(ReqID)&AIRLINE_NO=\(AIRLINE_NO)&ART_ID=\(AirlineComp)&PageNumber=\(PageNumber)&PageSize=" + pageSize)
//        api/Notifications/GetArrivalDepartureNotif?flightType={flightType}&UO_CODE={UO_CODE}&EA_CODE={EA_CODE}&TripDate={TripDate}&CityID={CityID}&ReqID={ReqID}&AIRLINE_NO={AIRLINE_NO}&ART_ID={ART_ID}&PageNumber={PageNumber}&PageSize={PageSize}
//        http://87.101.211.201/MobileAPI/Api/Notifications/GetArrivalDepartureNotif?flightType=-1&UO_CODE=10&EA_CODE=-1&TripDate=31-12-2018&CityID=-1&ReqID=-1&AIRLINE_NO=-1&ART_ID=&PageNumber=1&PageSize=10
        return url + "Api/Notifications/GetArrivalDepartureNotif?flightType=\(flightType)&UO_CODE=10&EA_CODE=\(ReqID)&TripDate=\(TripDate)&CityID=\(CityID)&ReqID=\(EA_CODE)&AIRLINE_NO=\(AIRLINE_NO)&ART_ID=&PageNumber=\(PageNumber)&PageSize=10"
    }
    
    
    
    public func getMazarat(TripDate:String,EA_CODE:String,CityID:String,PageNumber:String) -> String{
        print( url + "Api/Notifications/GetMazarat?EA_CODE=\(EA_CODE)&TripDate=\(TripDate)&CityID=\(CityID)&ART_ID=&PageNumber=\(PageNumber)&PageSize=10")
        return  url + "Api/Notifications/GetMazarat?EA_CODE=\(EA_CODE)&TripDate=\(TripDate)&CityID=\(CityID)&ART_ID=&PageNumber=\(PageNumber)&PageSize=10"
    }
    
    public func getPlaces(TripDate:String,EA_CODE:String,CityID:String,PageNumber:String) -> String{
        print( url + "api/Notifications/GetHousing?TripDate=\(TripDate)&EA_CODE=\(EA_CODE)&CityID=\(CityID)&PageNumber=\(PageNumber)&PageSize=" + pageSize)
        return  url + "api/Notifications/GetHousing?TripDate=\(TripDate)&EA_CODE=\(EA_CODE)&CityID=\(CityID)&PageNumber=\(PageNumber)&PageSize=10"
    }
    
    
    
    public func getFlights(flightType:String,AirlineCode:String,TripDate:String,FlightNO:String,TripStatus:String,TripCountry:String,TripCity:String,PageNumber:String) -> String{
//        print( url + "api/Notifications/GetFlights?flightType=\(flightType)&AirlineCode=\(AirlineCode)&TripDate=\(TripDate)&FlightNO=\(FlightNO)&TripStatus=\(TripStatus)&TripCountry=\(TripCountry)&TripCity=\(TripCity)&PageNumber=\(PageNumber)&PageSize=" + pageSize)
//        return  url + "api/Notifications/GetFlights?flightType=\(flightType)&AirlineCode=\(AirlineCode)&TripDate=\(TripDate)&FlightNO=\(FlightNO)&TripStatus=\(TripStatus)&TripCountry=\(TripCountry)&TripCity=\(TripCity)&PageNumber=\(PageNumber)&PageSize=" + pageSize
       return url + "Api/Notifications/GetFlights?flightType=\(flightType)&UO_CODE=10&EA_CODE=-1&TripDate=\(TripDate)&TripCity=\(TripCity)&FlightNO=\(FlightNO)&AirlineCode=\(AirlineCode)&TripStatus=\(TripStatus)&TripCountry=\(TripCountry)&PageNumber=\(PageNumber)&PageSize=10"
    }
    
    public func getNotifications(PageNumber:String) -> String{
        print( url + "api/Notifications/GetMandobNotification?PageNumber=\(PageNumber)&PageSize=" + pageSize)
        return  url + "api/Notifications/GetMandobNotification?PageNumber=\(PageNumber)&PageSize=" + pageSize
    }
    
    
    public func Countries() -> String{
        return  url + "api/Lookup/GetCountry"
    }
    
    public func ART() -> String{
        return  url + "api/Lookup/GetAirlineList"
    }
    
    public func Cities(id:Int) -> String{
        print( url + "api/Lookup/GetCity/\(id)")
        return  url + "api/Lookup/GetCity/\(id)"
    }
    
    public func Agents() -> String{
        return  url + "api/Lookup/GetAgent"
    }
    
    
    public func ProfileInfo() -> String{
        return  url + "api/Notifications/GetRepresentativeInfo"
    }
    
    public func getMo3tamereen(id:Int) -> String {
        print( url + "api/Notifications/GetMutamerList?P_ARN_ID=\(id)")
        return  url + "api/Notifications/GetMutamerList?P_ARN_ID=\(id)"
    }
    public func ChangePass() -> String{
        return  url + "api/Notifications/UpdateRepresentativePassword"
    }
    
    public func confirmArriavl() -> String{
        return  url + "api/Notifications/ConfirmArrival"
    }
    
    public func confirmDeparture() -> String{
        return  url + "api/Notifications/ConfirmDeparture"
    }
    
    public func CitiesForUser() -> String{
        print(url + "api/Lookup/GetNotificationCity")
        return url + "api/Lookup/GetNotificationCity"
    }
    
    public func Edit() -> String{
        return url + "api/Notifications/UpdateRepresentativeSettings"
    }
}
