//
//  Notifications.swift
//  Mandoop
//
//  Created by Moaz Ezz on 9/19/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation
class Notification: Decodable {
    var NOTIFICATION_ID : Int?
    var NOTIFICATION_ITEM_ID : Int?
    var NOTIFICATION_TYPE_ID : Int?
    var NOTIFICATION_MESSAGE : String?
    var NOTIFICATION_MESSAGE_LA : String?
    var NOTIFICATION_TIME : String?
    var NOTIFICATION_UO : Int?
    var NOTIFICATION_REP_IDENTITY : Int?
    var NOTIFICATION_IS_SENT : Int?
    var NOTIFICATION_SENT_TIME : String?
    var NOTIFICATION_PUSH : Int?
    var PUSH_NOTIFICATION : Int?
    var ONE_SIGNAL_ID : String?
}

