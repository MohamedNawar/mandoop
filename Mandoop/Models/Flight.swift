//
//  Flight.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation



class Flight : Decodable {
    var AT_NAME_AR : String?
    var FLIGHT_NO : String?
    var DATE_TRIP : String?
    var CNTRY_NAME_AR : String?
    var ORIGIN_CITY : String?
    var DESTINATION_CITY : String?
    var PORT : String?
    var TRIP_STATUS : String?
    var AT_NAME_LA : String?
    var ORIGIN_CITY_LA : String?
    var CNTRY_NAME_LA : String?
    var DESTINATION_CITY_LA : String?
    var PORT_LA : String?
    var TERMINAL : String?
    var TERMINAL_LA : String?
    var TRIP_STATUS_LA : String?
    var TRANS_TYPE : String?



}



