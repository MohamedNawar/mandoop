//
//  Error.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/19/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import Foundation

class ErrorHandler : Decodable{
    var error : String?
    var error_description : String?
    var Message : String?
//    func parseError() -> String {
//        var str = ""
//        if let temp = self.errors?.email{
//            str.append(temp[0])
//        }
//        if let temp = self.errors?.name{
//            str.append(temp[0])
//        }
//        if let temp = self.errors?.password{
//            str.append(temp[0])
//        }
//        if let temp = self.errors?.image_base64{
//            str.append(temp[0])
//        }
//        if let temp = self.errors?.expected_date{
//            str.append(temp[0])
//        }
//        if let temp = self.errors?.notify_at{
//            str.append(temp[0])
//        }
//        return str
//    }
    
    
}
struct ErrorImd : Decodable {
    var errors : [ErrorTypes]?
}
struct ErrorTypes : Decodable {
    var email : [String]?
    var password : [String]?
    var name : [String]?
    var notify_at : [String]?
    var expected_date : [String]?
    var image_base64 : [String]?
}
