//
//  Mo3tameer.swift
//  Mandoop
//
//  Created by Moaz Ezz on 8/19/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation

class Mo3tameer : Decodable {
    var BAM_MUTAMERID : Int?
    var EE_ENTRY_DATE : String?
    var EE_EXIT_DATE : String?
    var M_FULL_NAME : String?
    var M_PASSPORT_NO : String?
    var NT_NAME_AR : String?
    var NT_NAME_LA : String?
    var VISA_NO : String?
    var isSelected : Bool?
}
