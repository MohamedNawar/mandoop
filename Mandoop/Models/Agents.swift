//
//  Agents.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/11/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation
import Alamofire
import MOLH
class Agents {
    static let sharedInstance = Agents()
    private init() {}
    
    private var agents = [Agent]()
    
    public func getAgents() -> [Agent]{
        return agents
    }
    public func getAgentsNameArr(lang : Bool) -> [String]{
        
        var temp = [String]()
        if lang{//true = Arabic
            for i in agents{
                temp.append(i.AgentAr ?? "")
            }
        }else{//false = English
            for i in agents{
                temp.append(i.AgentLa ?? "")
            }
        }
        return temp
        
    }
    
    public func getAgentsAgentCode(name : String) -> String{
        for i in agents{
            if i.AgentAr == name{
                return "\(i.AgentCode ?? -1)"
            }
            if i.AgentLa == name{
                return "\(i.AgentCode ?? -1)"
            }
        }
        return ""
    }
    
    public func getAgentsCommercialRegNo(name : String) -> String{
        for i in agents{
            if i.AgentAr == name{
                return "\(i.CommercialRegNo ?? "")"
            }
            if i.AgentLa == name{
                return "\(i.CommercialRegNo ?? "")"
            }
        }
        return ""
    }
    
    public func getAgentName(code : Int,lang : Bool) -> String{
        for i in agents{
            if i.AgentCode == code{
                if lang {
                    return "\(i.AgentAr ?? "")"
                }else{
                    return "\(i.AgentLa ?? "")"
                }
                
            }
        }
        return ""
    }
    
    public func getAgentsServer() {
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        Alamofire.request(APIs.sharedInstance.Agents(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print("error from the server")
                    
                }else{
                    
                    do {
                        self.agents = try JSONDecoder().decode([Agent].self, from: response.data!)
                        
                        
                    }catch{
                        print("error in parsing the data")
                        
                    }
                }
            case .failure(_):
                print("حدث خطأ برجاء إعادة المحاولة لاحقاً")
                break
            }
        }
        
        
    }
    
    
}

class Agent : Decodable {
    
    var AgentCode : Int?
    var AgentAr : String?
    var AgentLa : String?
    var CountryId : Int?
    var Iata : String?
    var Address : String?
    var Phone : String?
    var FreePhone : String?
    var Email : String?
    var Url : String?
    var PoBox : String?
    var CommercialRegNo : String?
    var ResponsibleName : String?
    var ResponsibleMobile : String?
    var ResponsiblePassport : String?
    var NoOwners : Int?
    var Capital : Int?
    var Status : Int?
    
}

