//
//  AirLine.swift
//  Mandoop
//
//  Created by Moaz Ezz on 9/21/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation
import Alamofire
import PKHUD
import MOLH

class AirLines : Decodable {
    static let sharedInstance = AirLines()
    private init() {}
    
    private var airLines = [AirLine]()
    
    public func getAirLines() -> [AirLine]{
        return airLines
    }
    public func getAirlinesNameArr(lang : Bool) -> [String]{
        
        var temp = [String]()
        if lang{//true = Arabic
            for i in airLines{
                temp.append(i.AT_NAME_AR ?? "")
            }
        }else{//false = English
            for i in airLines{
                temp.append(i.AT_NAME_LA ?? "")
            }
        }
        return temp
        
    }
    
    public func getAirLinesId(name : String) -> String{
        for i in airLines{
            if i.AT_NAME_AR == name{
                return "\(i.AT_ID ?? -1)"
            }
            if i.AT_NAME_LA == name{
                return "\(i.AT_ID ?? -1)"
            }
        }
        return "-1"
    }
    
    public func getAirlineName(id : Int,lang : Bool) -> String{
        for i in airLines{
            if i.AT_ID == id{
                if lang {
                    return "\(i.AT_NAME_AR ?? "")"
                }else{
                    return "\(i.AT_NAME_LA ?? "")"
                }
                
            }
        }
        return ""
    }
    
    
    public func getAirlinesServer() {
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        
        HUD.show(.progress)
        Alamofire.request(APIs.sharedInstance.ART(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print("error from the server")
                    
                }else{
                    
                    do {
                        self.airLines = try JSONDecoder().decode([AirLine].self, from: response.data!)
                        
                        
                    }catch{
                        print("error in parsing the data")
                        
                    }
                }
                HUD.hide()
            case .failure(_):
                print("حدث خطأ برجاء إعادة المحاولة لاحقاً")
                HUD.hide()
                break
            }
        }
        
        
    }
    
    
}

class AirLine : Decodable {
    
    var CountryId : Int?
    var AT_ID : Int?
    var AT_NAME_AR : String?
    var AT_NAME_LA : String?
    
    
}
