//
//  UserData.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/17/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import Foundation


struct userData : Decodable {

    var access_token : String?
    var token_type : String?
    var expires_in : Int?
    var userName : String?
    var roleName : String?
    var UO : String?
    var EA : String?
    var playerId:String?
    
    
    
    func saveUser() {
        UserDefaults.standard.set(playerId, forKey: "onesignalid")
        UserDefaults.standard.set(access_token, forKey: "access_token")
        UserDefaults.standard.set(token_type, forKey: "token_type")
        UserDefaults.standard.set(expires_in, forKey: "expires_in")
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.set(roleName, forKey: "roleName")
        UserDefaults.standard.set(UO, forKey: "UO")
        UserDefaults.standard.set(EA, forKey: "EA")
    }
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "onesignalid")
        UserDefaults.standard.removeObject(forKey: "access_token")
        UserDefaults.standard.removeObject(forKey: "token_type")
        UserDefaults.standard.removeObject(forKey: "expires_in")
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "roleName")
        UserDefaults.standard.removeObject(forKey: "UO")
        UserDefaults.standard.removeObject(forKey: "EA")
        self.access_token = ""
        self.token_type = ""
        self.expires_in = -1
        self.userName = ""
        self.roleName = ""
        self.UO = ""
        self.EA = ""
        
    }
    mutating func fetchUser(){
        if let temp = UserDefaults.standard.object(forKey: "access_token" ) as? String{
            access_token = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "token_type" ) as? String{
            token_type = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "expires_in" ) as? Int{
            expires_in = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "userName" ) as? String{
            userName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "roleName" ) as? String{
            roleName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "UO" ) as? String{
            roleName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "EA" ) as? String{
            roleName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "onesignalid" ) as? String{
            playerId = temp
        }
        
    }
}

struct DetailedData : Decodable {
    var email : String?
    var gratitude : String?
    var gratitude_expire_at : String?
    var id : Int?
    var name : String?
    var photo : PhotoFormat?
    var image : PhotoFormat?
    var user_id : Int?
    var message : String?
}

struct PhotoFormat : Decodable {
    var large : String?
    var medium : String?
    var original : String?
    var small : String?
    var thumb : String?

}
