//
//  Countries.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation
import Alamofire
import MOLH
class Countries {
    static let sharedInstance = Countries()
    private init() {}
    
    private var countries = [Country]()
    
    public func getCities() -> [Country]{
        return countries
    }
    public func getCountriesNameArr(lang : Bool) -> [String]{
        
        var temp = [String]()
        if lang{//true = Arabic
            for i in countries{
                temp.append(i.CountryAr ?? "")
            }
        }else{//false = English
            for i in countries{
                temp.append(i.CountryLa ?? "")
            }
        }
        return temp
        
    }
    
    public func getCountriesId(name : String) -> String{
        for i in countries{
            if i.CountryAr == name{
                return "\(i.CountryId ?? -1)"
            }
            if i.CountryLa == name{
                return "\(i.CountryId ?? -1)"
            }
        }
        return "-1"
    }
    
    public func getCountryName(id : Int,lang : Bool) -> String{
        for i in countries{
            if i.CountryId == id{
                if lang {
                    return "\(i.CountryAr ?? "")"
                }else{
                    return "\(i.CountryLa ?? "")"
                }
                
            }
        }
        return ""
    }
    
    public func getCountriesServer() {
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        
        Alamofire.request(APIs.sharedInstance.Countries(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print("error from the server")
                    
                }else{
                    
                    do {
                        self.countries = try JSONDecoder().decode([Country].self, from: response.data!)
                        
                        
                    }catch{
                        print("error in parsing the data")
                        
                    }
                }
            case .failure(_):
                print("حدث خطأ برجاء إعادة المحاولة لاحقاً")
                break
            }
        }
        
        
    }
    
    
}

class Country : Decodable {
    
    var CountryId : Int?
    var CountryAr : String?
    var CountryLa : String?
    var Status : Int?
    
}
