//
//  FlightsVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import YNDropDownMenu
import FCAlertView
import MOLH 
class FlightsVC: UIViewController  , UITableViewDelegate, UITableViewDataSource {
    
    
    var pageCounter = 1
    var shouldGetMore = true
    var dateTemp = ""
    var flightType = "-1"
    var AirlineCode = "-1"
    var FlightNO = ""
    var TripStatus = "0"
    var TripCity = "-1"
    var TripCountry = "-1"

    var FlightArr = [Flight](){
        didSet{
            tableView.reloadData()
        }
    }
    var filterBtnPreesed = false
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var ActivityIndicatour: UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Flight Details", comment: "رحلات الطيران")
        dateTemp = getDate()
//        getFlights(flightType: flightType, AirlineCode: AirlineCode, TripDate: dateTemp, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: "\(pageCounter)")
        
        //        self.navigationController?.navigationBar.barTintColor = .clear
        // Do any additional setup after loading the view.
        
        pageCounter = 1
        getFlightsWithoutUi(flightType: flightType, AirlineCode: AirlineCode, TripDate: dateTemp, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: "\(pageCounter)")
        
    }
    
    
    @IBAction func filterBtn(_ sender: Any) {
        let popUpVC = storyboard?.instantiateViewController(withIdentifier: "FlightPopUp") as! FlightPopUp
        popUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popUpVC.delegate = self
        popUpVC.typeTemp = flightType
        popUpVC.dateTemp = dateTemp
        popUpVC.CityTemp = TripCity
        popUpVC.CountryTemp = TripCountry
        popUpVC.statusTemp = TripStatus
        popUpVC.FlightComTemp = AirlineCode
        popUpVC.FlightNO = FlightNO
        
        
        present(popUpVC, animated: true, completion: nil)
        
    }
    
//    private func setupViews(flag:Bool){
//        var ZBdropDownViews = Bundle.main.loadNibNamed("FlightFilterView", owner: nil, options: nil) as? [UIView]
//        let x = ZBdropDownViews![0] as! FlightFilterView
//        x.delegateTemp = self
//        ZBdropDownViews![0] = x
//        if let _ZBdropDownViews = ZBdropDownViews {
//            // Inherit YNDropDownView if you want to hideMenu in your dropDownViews
//
//            let view = YNDropDownMenu(frame: CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 0), dropDownViews: _ZBdropDownViews, dropDownViewTitles: [""])
//
//
//            // Add custom blurEffectView
//            let backgroundView = UIView()
//            backgroundView.backgroundColor = .black
//            view.blurEffectView = backgroundView
//            view.blurEffectViewAlpha = 0.7
//
//            // Open and Hide Menu
//            view.alwaysSelected(at: 0)
//
//            if flag {
//                self.view.addSubview(view)
//                view.showAndHideMenu(at: 0)
//                filterBtnPreesed = false
//            }else{
//                view.hideMenu()
//            }
//
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FlightArr.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.FlightArr.count - 1) && shouldGetMore{
            ActivityIndicatour.startAnimating()
            getFlightsWithoutUi(flightType: flightType, AirlineCode: AirlineCode, TripDate: dateTemp, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: "\(pageCounter)")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("FlightCell2", owner: self, options: nil)?.first as! FlightCell2
        cell.UISetup(arrival: self.FlightArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "Mo3tamereenVC") as! Mo3tamereenVC
//        vc.arrivaltemp = arrivalArr[indexPath.row]
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
//
//    func getFlights(flightType: String, AirlineCode: String, TripDate: String, FlightNO: String, TripStatus: String , TripCountry: String, TripCity: String, PageNumber: String){
//
//        FlightArr.removeAll()
//
//        let header = [
//            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
//            "Accept" : "application/json"
//            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
//        ]
//        HUD.show(.progress, onView: self.view)
//        print(APIs.sharedInstance.getFlights(flightType: flightType, AirlineCode: AirlineCode, TripDate: TripDate, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: PageNumber))
//        Alamofire.request("http://87.101.211.201/MobileAPI/Api/Notifications/GetFlights?flightType=-1&UO_CODE=10&EA_CODE=-1&TripDate=12-31-2018&TripCity=-1&FlightNO=&AirlineCode=-1&TripStatus=0&TripCountry=-1&PageNumber=1&PageSize=10", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//
//
//
//            switch(response.result) {
//            case .success(let value):
//                print(response.value)
//                print(response.result.error)
//                print(response.response?.statusCode)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
//                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                        //                            user.saveUser(user: user)
//                    }catch{
//                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//
//                    do {
//                        self.FlightArr = try JSONDecoder().decode([Flight].self, from: response.data!)
//                        self.tableView.reloadData()
//
//                    }catch{
//                        var ftemp = "No avaiable flights with this date"
//                        if Language.sharedInstance.getLang(){
//                            ftemp = "لا يوجد رحلات بهذا التاريخ"
//                        }
//                        HUD.flash(.label(ftemp), delay: 1.0)
//                        self.tableView.reloadData()
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    
    func getFlightsWithoutUi(flightType: String, AirlineCode: String, TripDate: String, FlightNO: String, TripStatus: String , TripCountry: String, TripCity: String, PageNumber: String){
        
        
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        print(user.access_token)
        //        HUD.show(.progress, onView: self.view)
        print(APIs.sharedInstance.getFlights(flightType: flightType, AirlineCode: AirlineCode, TripDate: TripDate, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: PageNumber))
        Alamofire.request(APIs.sharedInstance.getFlights(flightType: flightType, AirlineCode: AirlineCode, TripDate: TripDate, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                //                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode([Flight].self, from: response.data!)
                        if temp.count == 0 {
                            self.shouldGetMore = false
                            self.ActivityIndicatour.stopAnimating()
                        }else{
                            self.FlightArr.append(contentsOf: temp)
                            self.tableView.beginUpdates()
                            
//                            self.tableView.insertRows(at: [IndexPath(row: self.FlightArr.count-1, section: 0)], with: .automatic)
                            
                            self.tableView.endUpdates()
                            self.pageCounter += 1
                        }
                    }catch{
                        //                        var ftemp = "No avaiable flights with this date"
                        //                        if Language.sharedInstance.getLang(){
                        //                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        //                        }
                        self.shouldGetMore = false
                        self.ActivityIndicatour.stopAnimating()
                        //                        HUD.flash(.label(ftemp), delay: 1.0)
                        self.tableView.reloadData()
                    }
                }
            case .failure(_):
                //                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    private func getDate() -> String{//return date as formated -> year-day-month 11-15-2017
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
         return "\(month)-\(day)-\(year)"
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
    
}

extension FlightsVC : FlightPopUpDelegte{
    
    func popUpValues(flightType: String, AirlineCode: String, TripDate: String, FlightNO: String, TripStatus: String, TripCountry: String, TripCity: String, PageNumber: String) {
        filterBtnPreesed = false
        
        
        self.flightType = flightType
        self.AirlineCode = AirlineCode
        self.dateTemp = TripDate
        self.FlightNO = FlightNO
        self.TripStatus = TripStatus
        self.TripCity = TripCity
        self.TripCountry = TripCountry
        shouldGetMore = true
        getFlightsWithoutUi(flightType: flightType, AirlineCode: AirlineCode, TripDate: TripDate, FlightNO: FlightNO, TripStatus: TripStatus, TripCountry: TripCountry, TripCity: TripCity, PageNumber: PageNumber)
    }
    
    func justBeingCancelled() {
        filterBtnPreesed = false
    }
    
    
}
