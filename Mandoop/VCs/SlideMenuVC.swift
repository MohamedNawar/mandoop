//
//  SlideMenuVC.swift
//  Mazen
//
//  Created by Moaz Ezz on 3/18/18.
//  Copyright © 2018 ElnoorOnline. All rights reserved.
//

import UIKit
import MOLH

class SlideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var imagesArray = [#imageLiteral(resourceName: "img07"),#imageLiteral(resourceName: "img08"),#imageLiteral(resourceName: "img09"),#imageLiteral(resourceName: "translate"),#imageLiteral(resourceName: "img10"),#imageLiteral(resourceName: "img11")]
//    var nameArray = ["الرئيسية","الملف الشخصي","الإشعارات","تغير اللغة","تسجيل الخروج"]
    
    var nameArray = [NSLocalizedString("Main", comment: "الرئيسية"),
                     NSLocalizedString("Profile", comment: "الملف الشخصي"),
                     NSLocalizedString("Notifications", comment: "الإشعارات"),
                     NSLocalizedString("Change Language", comment: "تغير اللغة"),
                     NSLocalizedString("Settings", comment: "الإعدادات"),
                     NSLocalizedString("Sign Out", comment: "تسجيل الخروج")]
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableHeaderView = headerView
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuCell")!
        
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(-2) as! UILabel
        
        image.image = imagesArray[indexPath.row]
        lbl.text = nameArray[indexPath.row]
        
        return cell
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case 1:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC2") as! ProfileVC2
            self.navigationController?.pushViewController(navVC, animated: true)
        case 2:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
            self.navigationController?.pushViewController(navVC, animated: true)

        case 3:
            
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
        
        self.dismiss(animated: false, completion: nil)
        if let vc = storyboard?.instantiateViewController(withIdentifier: "rootnav") {
            UIApplication.shared.keyWindow?.rootViewController = vc
            }
        self.loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: [self.view])
       
//        MOLH.reset()
//        case 4:
//            let navVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC2") as! AboutUsVC2
//            self.navigationController?.pushViewController(navVC, animated: true)
        case 4:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case 5:
            user.remove()
            user.fetchUser()
            
            let navVC = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            self.present(navVC, animated: true, completion: nil)
            
        default:
            break
        }
    }
    
    
    
}

extension UIViewController {
    func loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: [UIView]) {
        
        if subviews.count > 0 {
            for subView in subviews {
                print("flipping...")
                if subView.isKind(of:UIButton.self) {
                    let toRightArrow = subView as! UIButton
                    if let _img = toRightArrow.currentImage {
                        let flippedImg = UIImage(cgImage: _img.cgImage!, scale: _img.scale, orientation:  UIImageOrientation.upMirrored)
                        
                        /*1*/toRightArrow.setImage(flippedImg, for: .normal)
                    }
                }
                /*2*/loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: subView.subviews)
            }
        }
    }
}
