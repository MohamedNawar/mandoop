//
//  ArrivalVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/10/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import YNDropDownMenu
import FCAlertView
import MOLH
class ArrivalVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var pageCounter = 1
    var shouldGetMore = true
    
    var typeTemp = "-1"
    var dateTemp = ""
    var CityTemp = "-1"
    var flightCompTemp = "-1"
    var AgentTemp = "-1"
    var EATemp = "-1"
    var flightNumTemp = "-1"
    
    
    
    
    var arrivalArr = [Arrival](){
        didSet{
            tableView.reloadData()
        }
    }
    var filterBtnPreesed = false
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var ActivityIndicatour: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Arrival And Departures", comment: "رحلات الوصول والمغادرة")
       EATemp = user.EA ?? "-1"
        dateTemp = getDate()
//        getArrivalsWithoutUi(flightType: typeTemp, EA_CODE: EATemp, TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AirlineComId: flightCompTemp, AIRLINE_NO: flightNumTemp, PageNumber: "\(pageCounter)")
        
//        self.navigationController?.navigationBar.barTintColor = .clear
        // Do any additional setup after loading the view.
        pageCounter = 1
        getArrivalsWithoutUi(flightType: typeTemp, EA_CODE: EATemp, TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AirlineComId: flightCompTemp, AIRLINE_NO: flightNumTemp, PageNumber: "\(pageCounter)")
        
        
    }
 
    @IBAction func filterBtn(_ sender: Any) {
//        if !filterBtnPreesed{
//            setupViews(flag:true)
//            filterBtnPreesed = true
//        }
        let popUpVC = storyboard?.instantiateViewController(withIdentifier: "ArrivalAndDeparturePopUp") as! ArrivalAndDeparturePopUp
        popUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popUpVC.delegate = self
        
   
        popUpVC.typeTemp = typeTemp
        popUpVC.dateTemp = dateTemp
        popUpVC.CityTemp = CityTemp
        popUpVC.FlightComTemp = flightCompTemp
        popUpVC.AgentTemp = AgentTemp
        popUpVC.EATemp = EATemp
        popUpVC.flightNumTemp = flightNumTemp
        
        present(popUpVC, animated: true, completion: nil)
        
    }
    
//    private func setupViews(flag:Bool){
//        var ZBdropDownViews = Bundle.main.loadNibNamed("DropDownViews", owner: nil, options: nil) as? [UIView]
//        let x = ZBdropDownViews![0] as! ZBFilterFeatureView
//        x.delegateTemp = self
//        ZBdropDownViews![0] = x
//        if let _ZBdropDownViews = ZBdropDownViews {
//            // Inherit YNDropDownView if you want to hideMenu in your dropDownViews
//
//            let view = YNDropDownMenu(frame: CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 0), dropDownViews: _ZBdropDownViews, dropDownViewTitles: [""])
//
//
//            // Add custom blurEffectView
//            let backgroundView = UIView()
//            backgroundView.backgroundColor = .black
//            view.blurEffectView = backgroundView
//            view.blurEffectViewAlpha = 0.7
//
//            // Open and Hide Menu
//            view.alwaysSelected(at: 0)
//
//            if flag {
//                self.view.addSubview(view)
//                view.showAndHideMenu(at: 0)
//                filterBtnPreesed = false
//            }else{
//                view.hideMenu()
//            }
//
//        }
//    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(arrivalArr.count)
        return arrivalArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("flieghtCell", owner: self, options: nil)?.first as! flieghtCell
        cell.UISetup(arrival: self.arrivalArr[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.arrivalArr.count - 1) && shouldGetMore  {
            ActivityIndicatour.startAnimating()
            getArrivalsWithoutUi(flightType: typeTemp, EA_CODE: EATemp, TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AirlineComId: flightCompTemp, AIRLINE_NO: flightNumTemp, PageNumber: "\(pageCounter)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Mo3tamereenVC") as! Mo3tamereenVC
       vc.arrivaltemp = arrivalArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    
//
//    func getArrivals(flightType: String, EA_CODE: String, TripDate: String, CityID: String, ReqID: String,AirlineComId: String ,AIRLINE_NO: String, PageNumber: String){
//        pageCounter = 1
//        arrivalArr.removeAll()
//
//        let header = [
//            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
//            "Accept" : "application/json"
//            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
//        ]
//        print(user.access_token)
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.sharedInstance.getArrivalAndDeparture(flightType: flightType, EA_CODE: EA_CODE, TripDate: TripDate, CityID: CityID, ReqID: ReqID, AirlineComp: AirlineComId, AIRLINE_NO: AIRLINE_NO, PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//
//
//
//            switch(response.result) {
//            case .success(let value):
//                print(response.value)
//                print(response.result.error)
//                print(response.response?.statusCode)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
//                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                        //                            user.saveUser(user: user)
//                    }catch{
////                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//
//                    do {
//                        self.arrivalArr = try JSONDecoder().decode([Arrival].self, from: response.data!)
//                        self.tableView.reloadData()
//                        self.pageCounter += 1
//
//                    }catch{
//                        var ftemp = "No avaiable flights with this date"
//                        if Language.sharedInstance.getLang(){
//                            ftemp = "لا يوجد رحلات بهذا التاريخ"
//                        }
//                        HUD.flash(.label(ftemp), delay: 1.0)
//                        self.tableView.reloadData()
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    
    
    func getArrivalsWithoutUi(flightType: String, EA_CODE: String, TripDate: String, CityID: String, ReqID: String,AirlineComId: String ,AIRLINE_NO: String, PageNumber: String){
        
        
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        print(user.access_token)
        print(APIs.sharedInstance.getArrivalAndDeparture(flightType: flightType, EA_CODE: EA_CODE, TripDate: TripDate, CityID: CityID, ReqID: ReqID, AirlineComp: AirlineComId, AIRLINE_NO: AIRLINE_NO, PageNumber: PageNumber))
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getArrivalAndDeparture(flightType: flightType, EA_CODE: EA_CODE, TripDate: TripDate, CityID: CityID, ReqID: ReqID, AirlineComp: AirlineComId, AIRLINE_NO: AIRLINE_NO, PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode([Arrival].self, from: response.data!)
                        if temp.count == 0 {
                            self.shouldGetMore = false
                            self.ActivityIndicatour.stopAnimating()
                        }else{
                            self.arrivalArr.append(contentsOf: temp)
                            self.tableView.beginUpdates()
                            
//                            self.tableView.insertRows(at: [IndexPath(row: self.arrivalArr.count-1, section: 0)], with: .automatic)
                            
                            self.tableView.endUpdates()
                            self.pageCounter += 1
                        }
                        
                    }catch{
//                        var ftemp = "No avaiable flights with this date"
//                        if Language.sharedInstance.getLang(){
//                            ftemp = "لا يوجد رحلات بهذا التاريخ"
//                        }
                        self.shouldGetMore = false
                        self.ActivityIndicatour.stopAnimating()
//                        HUD.flash(.label(ftemp), delay: 1.0)
                        self.tableView.reloadData()
                    }
                }
            case .failure(_):
//                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    private func getDate() -> String{//return date as formated -> year-day-month 11-15-2017
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
         return "\(day)-\(month)-\(year)"
    }

   
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }

}

extension ArrivalVC : ArrivalsPopUpDelegte{
    
    func popUpValues(flightType: String, EA_CODE: String, TripDate: String, CityID: String, ReqID: String, AIRLINE_NO: String, PageNumber: String, country : String) {
        filterBtnPreesed = false
        typeTemp = flightType
        EATemp = EA_CODE
        dateTemp = TripDate
        CityTemp = CityID
        AgentTemp = ReqID
        flightNumTemp = AIRLINE_NO
        flightCompTemp = country
        shouldGetMore = true
        
        getArrivalsWithoutUi(flightType: typeTemp, EA_CODE: EATemp, TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AirlineComId: flightCompTemp, AIRLINE_NO: flightNumTemp, PageNumber: "\(pageCounter)")
//        getArrivals(flightType: flightType, EA_CODE: EA_CODE, TripDate: TripDate, CityID: CityID, ReqID: ReqID, AIRLINE_NO: AIRLINE_NO, PageNumber: PageNumber)
    }
    
    func justBeingCancelled() {
        filterBtnPreesed = false
    }
    
    
    

}
