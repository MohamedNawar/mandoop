//
//  testViewController.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import CoreLocation

class testViewController: UIViewController , CLLocationManagerDelegate{
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setupLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        navVC.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    func setupLocation(){
        
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }

}
