//
//  SingInVC.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/17/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import UIKit

import FCAlertView
import Alamofire
import PKHUD
import Spring
import Font_Awesome_Swift
import DCKit
var user = userData()
class LogInVC: UIViewController {
    

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBAction func sendMessage(_ sender: Any) {
        login()
    }
    
    @IBAction func goToSignUp(_ sender: Any) {
        
    }
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phone.placeholder = NSLocalizedString("Identity No.", comment: "رقم العضوية")
        self.password.placeholder = NSLocalizedString("Password", comment: "كلمة السر")

        
    }

    
    
    
    

    
    
    
   
    
    func login(){
        
        
        if phone.text!.isEmpty == false && password.text!.isEmpty == false {
//            if !(email.text?.isValidEmail())!{
//                makeDoneAlert(title: "البريد الإلكتروني غير صحيح", SubTitle: "من فضلك تأكد من إدخال البريد الإلكتروني بطريقة صحيحة", Image: #imageLiteral(resourceName: "logo"))
//                return
//            }
            
            let header = [
                "Content-Type" : "application/x-www-form-urlencoded"
            ]
            let par = ["grant_type": "password",
                       "username": phone.text!,
                       "password": password.text!,
                       "onesignal-player-id": user.playerId ?? ""
            ]
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.sharedInstance.LogIn(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(response.value)
                    print(response.result.error)
                    print(response.response?.statusCode)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                       
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                            HUD.flash(.labeledError(title: err.error_description, subtitle: err.error_description), delay: 1.0)
                            self.makeDoneAlert(title: err.error_description ?? "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: err.error_description ?? "", Image: #imageLiteral(resourceName: "Logo_blue"))
                            //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                            user.saveUser(user: user)
                            
                        }catch{
                            HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }else{
                        
                        do {
                            user = try JSONDecoder().decode(userData.self, from: response.data!)
                            HUD.flash(.success, delay: 1.0)
                            user.saveUser()
                            self.goToMain()
                        }catch{
                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                    break
                }
            }
        }else{
            print("do some checking here")
        }
    }
    
    func goToMain()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
}

@IBDesignable
class DesignableUITextField: DCBorderedTextField {
    
    // Provides left padding for images
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x += rightPadding
        return textRect
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = rightImage {
            rightViewMode = UITextFieldViewMode.always
            var width = rightPadding + 20
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width = width + 20
            }
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            
            imageView.tintColor = color
            rightView = imageView
        } else {
            rightViewMode = UITextFieldViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
    
    
}
