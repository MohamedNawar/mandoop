//
//  SettingVC.swift
//  Mandoop
//
//  Created by Mohamed Nawar on 8/26/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import McPicker
import MOLH


class SettingVC: UIViewController , UIPickerViewDelegate , UITextFieldDelegate  {
    var representative = Representative()
    //    let picker = UIPickerView()
    var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    var CityTemp = "-1"
    
    @IBOutlet weak var cityView: UIView!

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var updateCityLabel: UILabel!
    @IBOutlet weak var updateCity: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var getNotification: UILabel!
    @IBOutlet weak var state: UISwitch!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var extraView: UIView!
    @IBOutlet weak var ChooseCity: UITextField!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()

      if MOLHLanguage.currentAppleLanguage() == "ar" {
        updateCityLabel.textAlignment = .right
      }else{
        updateCityLabel.textAlignment = .left
        }
        getData()
    }
    
    @IBAction func switchValueChanged (sender: UISwitch) {
        //        advice.isInProduction = sender.on
        sendUpdate()
        print ("It's \(sender.isOn)!")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Cities.sharedInstance.getCitiesForUser()
        getData()
        
        cityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openPicker)))
        ChooseCity.addPadding(UITextField.PaddingSide.left(20))
        //         showCityPicker()
        ChooseCity.delegate = self
        self.title = NSLocalizedString("Settings", comment: "الإعدادات")
        self.saveBtn.setTitle(NSLocalizedString("Save", comment: "حفظ"), for: .normal)
        self.updateCity.setTitle(NSLocalizedString("Edit City", comment:         "تعديل المدينة"), for: .normal)
        self.getNotification.text = NSLocalizedString("get Notifications", comment: "الحصول علي آشعارات")
        self.cityLabel.text = NSLocalizedString("City :", comment: "المدينة :")
        self.cityNameLabel.text = NSLocalizedString("City", comment: "المدينة")
        self.updateCityLabel.text = NSLocalizedString("Edit City", comment:         "تعديل المدينة")
        if state.isOn {
            self.stateLabel.text = NSLocalizedString("On", comment:"نعم")
        }else{
            self.stateLabel.text = NSLocalizedString("Off", comment:"لا")
        }
        self.ChooseCity.placeholder = NSLocalizedString("Choose", comment: "أختر")
        
        
        
    }
    
    
    @objc private func openPicker(){
        
        let cotemp = Cities.sharedInstance.getCitiesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.ChooseCity.text = StringTemp
                    self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
                }
            }
        }else{
            if Language.sharedInstance.getLang(){
                HUD.flash(.label("يجب عليك اختيار البلد أولا"), delay: 1.0)
            }else{
                HUD.flash(.label("You have to choose Countery First"), delay: 1.0)
            }
            
        }
    }
    
    
    
    @objc func donePicker(){
        if ChooseCity.text == "" {
            ChooseCity.text = words[0]
        }
        self.view.endEditing(true)
    }
    //    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    //            return words.count
    //    }
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //            return "\(words[row])"
    //
    //    }
    //    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    //            selected = words[row]
    //            ChooseCity.text = selected
    //
    //
    //
    //    }
    
    
    func getData() {
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
        ]
        Alamofire.request(APIs.sharedInstance.ProfileInfo(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            if response.result.isSuccess {
                self.representative = try! JSONDecoder().decode(Representative.self, from: response.data!)
                self.cityNameLabel.text = Language.sharedInstance.getLang() ? self.representative.CT_NAME_AR : self.representative.CT_NAME_LA
                self.ChooseCity.text = Language.sharedInstance.getLang() ? self.representative.CT_NAME_AR : self.representative.CT_NAME_LA
                if self.representative.PUSH_NOTIFICATION == 1 {
                    self.stateLabel.text = NSLocalizedString("On", comment:"نعم")
                    self.state.isOn = true
                    self.stateLabel.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                }else{
                    self.stateLabel.text = NSLocalizedString("Off", comment:"لا")
                    self.state.isOn = false
                    self.stateLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }
                
            }else{
                var ftemp = "Something went wrong!! Please try again later"
                if Language.sharedInstance.getLang(){
                    ftemp = "حدث خطأ برجاء إعادة المحاولة لاحقاً"
                }
                HUD.flash(.labeledError(title: "", subtitle: ftemp), delay: 1.0)
                print("errorToGetWeatherData")
            }
        }
    }
    
    @IBAction func updateCity(_ sender: Any) {
        Cities.sharedInstance.getCitiesForUser()
        extraView.isHidden = false
    }
    
    
    private func sendUpdate(){
        if ChooseCity.text == "" {
            HUD.flash(.labeledError(title: "Error", subtitle: "Please Choose a City"), delay: 1.0)
        }else{
            var stateValue = state.isOn ? 1 : 0
            cityNameLabel.text = ChooseCity.text
            let cityId = Cities.sharedInstance.getCitiesId(name: ChooseCity.text!)
            let par:[String : Any] = ["city": cityId ,
                                      "push_notification": stateValue ]
            let header = [
                "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
                "Accept" : "application/json"
                , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
            ]
            HUD.show(.progress)
            Alamofire.request(APIs.sharedInstance.Edit(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                
                if response.result.isSuccess {
                    HUD.hide()
                    self.extraView.isHidden = true
                    
                }else{
                    var ftemp = "Something went wrong!! Please try again later"
                    if Language.sharedInstance.getLang(){
                        ftemp = "حدث خطأ برجاء إعادة المحاولة لاحقاً"
                    }
                    HUD.flash(.labeledError(title: "", subtitle: ftemp), delay: 1.0)
                    print("errorToGetWeatherData")
                }
            }
        }
    }
    @IBAction func saveBtn(_ sender: Any) {
        sendUpdate()
    }
    @IBAction func switchState(_ sender: Any) {
        if state.isOn {
            self.stateLabel.text = NSLocalizedString("On", comment:"موافقة")
            stateLabel.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }else{
            self.stateLabel.text = NSLocalizedString("Off", comment:"رفض")
            stateLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
    }
}
