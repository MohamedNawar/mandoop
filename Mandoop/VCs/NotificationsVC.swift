//
//  NotificationsVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/12/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import MOLH
class NotificationsVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var shouldGetMore = true
    var pageCounter = 1
//    var isGettingData = true
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var notifyArr = [Notification]()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Notifications", comment: "الإشعارات")
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "background_2"), for: .default)
       
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100.0
        
        //        self.navigationController?.navigationBar.barTintColor = .clear
        // Do any additional setup after loading the view.
        pageCounter = 1
        getNotificationsWithoutUi(PageNumber: "\(pageCounter)")
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifyArr.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.notifyArr.count - 1) && shouldGetMore {
            activityIndicator.startAnimating()
            getNotificationsWithoutUi(PageNumber: "\(pageCounter)")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("NotificationCell", owner: self, options: nil)?.first as! NotificationCell
        cell.UISetup(notify: self.notifyArr[indexPath.row])
        return cell
    }
    
    
//
//    func getNotifications(PageNumber: String){
//
//        notifyArr.removeAll()
//
//        let header = [
//            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
//            "Accept" : "application/json"
//            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
//        ]
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.sharedInstance.getNotifications(PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//
//
//
//            switch(response.result) {
//            case .success(let value):
//                print(response.value)
//                print(response.result.error)
//                print(response.response?.statusCode)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
//                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                        //                            user.saveUser(user: user)
//                    }catch{
//                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//
//                    do {
//                        self.notifyArr = try JSONDecoder().decode([Notification].self, from: response.data!)
//                        self.tableView.reloadData()
//
//                    }catch{
//                        var ftemp = "No avaiable flights with this date"
//                        if Language.sharedInstance.getLang(){
//                            ftemp = "لا يوجد رحلات بهذا التاريخ"
//                        }
//                        HUD.flash(.label(ftemp), delay: 1.0)
//                        self.tableView.reloadData()
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    
    
    
    func getNotificationsWithoutUi(PageNumber: String){
//        isGettingData = false
//        notifyArr.removeAll()
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getNotifications(PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode([Notification].self, from: response.data!)
                        if temp.count == 0 {
                            self.shouldGetMore = false
                            self.activityIndicator.stopAnimating()
                        }else{
                            self.notifyArr.append(contentsOf: temp)
                            self.tableView.beginUpdates()
                            
                            self.tableView.insertRows(at: [IndexPath(row: self.notifyArr.count-1, section: 0)], with: .automatic)
                            
                            self.tableView.endUpdates()
                        }
                        self.pageCounter += 1
                    }catch{
                        var ftemp = "No avaiable flights with this date"
                        if Language.sharedInstance.getLang(){
                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        }
                        HUD.flash(.label(ftemp), delay: 1.0)
                        self.tableView.reloadData()
                    }
                }
            case .failure(_):
//                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
    let alert = FCAlertView()
    alert.avoidCustomImageTint = true
    alert.colorScheme = ColorControl.sharedInstance.getMainColor()
    alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
    private func getDate() -> String{//return date as formated -> year-day-month 11-15-2017
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
          return "\(day)-\(month)-\(year)"
    }
    
    
    
}

