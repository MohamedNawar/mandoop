//
//  MazaratPopUp.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/20/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import McPicker
import ActionSheetPicker_3_0
import PKHUD
import MOLH
class MazaratPopUp2: UIViewController {
    
    var delegate: MazaratPopUpDelegte1?
    var typeTemp = "-1"
    var dateTemp = ""
    var CityTemp = "-1"
    var CounteryTemp = "-1"
    var AgentTemp = "-1"
    
    
    @IBOutlet weak var date1: UILabel!
    
    //    @IBOutlet weak var countery1: UILabel!
    
    @IBOutlet weak var city1: UILabel!
    
    @IBOutlet weak var ea1: UILabel!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLbl: UITextField!
    
    //    @IBOutlet weak var counteryView: UIView!
    //    @IBOutlet weak var counteryLbl: UITextField!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLbl: UITextField!
    
    @IBOutlet weak var EALbl: UITextField!
    @IBOutlet weak var EAView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if MOLHLanguage.currentAppleLanguage() == "en"{
            EALbl.addPadding(UITextField.PaddingSide.left(20))
            dateLbl.addPadding(UITextField.PaddingSide.left(20))
            cityLbl.addPadding(UITextField.PaddingSide.left(20))
            
            EALbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            dateLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            cityLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            
        }else{
            EALbl.addPadding(UITextField.PaddingSide.right(20))
            dateLbl.addPadding(UITextField.PaddingSide.right(20))
            cityLbl.addPadding(UITextField.PaddingSide.right(20))
            
            EALbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            dateLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            cityLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            
        }
        
        
        let date = Date()
        //        dateTemp = getDate(date: date)
        self.initViews()
        self.setuplang()
        setupView()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissSelf(_:))))
    }
    
    private func setupView(){
        
        
        self.dateLbl.text = dateTemp
        self.cityLbl.text = Cities.sharedInstance.getCityName(id: Int(CityTemp) ?? -1, lang: Language.sharedInstance.getLang())
        //        self.counteryLbl.text = Countries.sharedInstance.getCountryName(id: Int(CounteryTemp) ?? -1, lang: Language.sharedInstance.getLang())
        self.EALbl.text = Agents.sharedInstance.getAgentName(code: Int(AgentTemp) ?? -1, lang: Language.sharedInstance.getLang())
        
        //        self.EALbl.text = (AgentTemp == "-1") ? "" : AgentTemp
        //        self.flightNumLbl.text = (flightNumTemp == "-1") ? "" : flightNumTemp
        
        
    }
    
    @objc private func dismissSelf(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        if EALbl.text == ""{
            AgentTemp = "-1"
        }
        
        delegate?.popUpValues(TripDate: dateTemp, EA_CODE: AgentTemp, CityID: CityTemp, PageNumber: "1", country: CounteryTemp)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegate?.justBeingCancelled()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func initViews() {
        Cities.sharedInstance.getCitiesForUser()
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        Agents.sharedInstance.getAgentsServer()
        //        counteryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.counteryPressed(_:))))
        cityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        EAView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.EAPressed(_:))))
        
    }
    
    
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in
            
            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self.view)
        
    }
    @objc private func EAPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Agents.sharedInstance.getAgentsNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.EALbl.text = StringTemp
                    self.AgentTemp = Agents.sharedInstance.getAgentsAgentCode(name: StringTemp)
                    print(self.AgentTemp)
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Cities.sharedInstance.getCitiesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.cityLbl.text = StringTemp
                    self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
                }
            }
        }else{
            if Language.sharedInstance.getLang(){
                HUD.flash(.label("يجب عليك اختيار البلد أولا"), delay: 1.0)
            }else{
                HUD.flash(.label("You have to choose Countery First"), delay: 1.0)
            }
            
        }
    }
    
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        return "\(day)-\(month)-\(year)"
    }
    
    private func setuplang(){
        
        date1.text = NSLocalizedString("Date :", comment: "التاريخ :")
        //        countery1.text = NSLocalizedString("Countery :", comment: "الدولة :")
        city1.text = NSLocalizedString("City :", comment: "المدينة :")
        
        ea1.text = NSLocalizedString("EA :", comment: "الوكالة :")
        
        cancelBtn.setTitle(NSLocalizedString("Cancel", comment: "إلغاء"), for: .normal)
        okBtn.setTitle(NSLocalizedString("OK", comment: "ابحث"), for: .normal)
    }
    
    
    
}
