//
//  MainVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/7/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import CoreLocation
import SideMenu
import MOLH

class MainVC: UIViewController , CLLocationManagerDelegate{
    let locationManager = CLLocationManager()
    var SignUpFlag = false
    
    
    
    @IBOutlet weak var arrivalLbl: UILabel!
    
    @IBOutlet weak var mazartLbl: UILabel!
    
    @IBOutlet weak var housingLbl: UILabel!
    
    @IBOutlet weak var flieghtDetails: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var city: UILabel!
    
    
    @IBOutlet weak var arrivalAndBackView: UIView!
    
    @IBOutlet weak var visitorsView: UIView!
    @IBOutlet weak var accomndations: UIView!
    @IBOutlet weak var fleights: UIView!
    
    
    @IBAction func goToSideMenu(_ sender: Any) {
        
        print(MOLHLanguage.currentAppleLanguage())
        
        let VC = storyboard?.instantiateViewController(withIdentifier: "SlideMenuNavVC") as! UISideMenuNavigationController
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            SideMenuManager.default.menuRightNavigationController = VC
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
                
            }else{
            SideMenuManager.default.menuLeftNavigationController = VC
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            
                
            }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrivalLbl.text = NSLocalizedString("Arrival And Departures", comment: "رحلات الوصول و المغادرة")
        mazartLbl.text = NSLocalizedString("Mazarat", comment: "المزارات")
        flieghtDetails.text = NSLocalizedString("Flight Details", comment: "رحلات الطيران")
        housingLbl.text = NSLocalizedString("Housing", comment: "التسكين")
        self.title = NSLocalizedString("Main", comment: "الرئيسية")
        
        user.fetchUser()
        
        if user.access_token == "" || user.access_token == nil {
            let navVC = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            self.present(navVC, animated: false, completion: nil)
        }
        setupLocation()
        
        
        arrivalAndBackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.arrivalPressed(_:))))
        visitorsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.visitorsPressed(_:))))
        accomndations.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.accomndationsPressed(_:))))
        fleights.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.fleightsPressed(_:))))
        
        Countries.sharedInstance.getCountriesServer()
        Agents.sharedInstance.getAgentsServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    
    @objc private func arrivalPressed(_ sender: UITapGestureRecognizer){
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ArrivalVC") as! ArrivalVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @objc private func visitorsPressed(_ sender: UITapGestureRecognizer){
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MazartaVC") as! MazartaVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @objc private func accomndationsPressed(_ sender: UITapGestureRecognizer){
        let navVC = storyboard?.instantiateViewController(withIdentifier: "PlacesVC") as! PlacesVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @objc private func fleightsPressed(_ sender: UITapGestureRecognizer){
        let navVC = storyboard?.instantiateViewController(withIdentifier: "FlightsVC") as! FlightsVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }

    
    func setupLocation(){
        
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocation = manager.location else { return }
        fetchCityAndCountry(location: locValue) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.city.text = city + ", " + country// Rio de Janeiro, Brazil
        }
    }
    
    func fetchCityAndCountry(location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }


}

