//
//  mo3tamereenVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 8/18/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import MOLH
class Mo3tamereenVC: UIViewController {
    var mo3terren = [Mo3tameer]()
    var arrivaltemp = Arrival()
    
    @IBOutlet weak var BtnHeight: NSLayoutConstraint!
    var shouldGetMore = true
    var pageCounter = 1
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var btnoutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var isAllSeleced = false {
        didSet{
            for i in mo3terren{
                i.isSelected = isAllSeleced
            }
            checkBox.isSelected = isAllSeleced
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var checkBox: UIButton!
    
    @IBAction func checkBoxPressed(_ sender: Any) {
        isAllSeleced = !isAllSeleced
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    
    @IBAction func send(_ sender: Any) {
        var tempArr = [Mo3tameer]()
        for i in mo3terren{
            if i.isSelected ?? false{
                tempArr.append(i)
            }
        }
        postMo3tmeer(items:tempArr)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if arrivaltemp.TRANS_TYPE == nil {
            BtnHeight.constant = 0
            checkBox.isHidden = true
        }else if arrivaltemp.TRANS_TYPE == "ARRIVAL"{
            btnoutlet.setTitle(NSLocalizedString("Confirm arrival", comment: "تأكيد الوصول"), for: .normal)
        }else{
            btnoutlet.setTitle(NSLocalizedString("Confirm departure", comment: "تأكيد المغادرة"), for: .normal)
        }
        
        pageCounter = 1
        getMo3tamreenWithoutUi(id:arrivaltemp.ARN_ID ?? -1)
        
//        temp = [false,true,true,false,false]
        checkBox.layer.borderWidth = 1
        checkBox.layer.borderColor = UIColor.darkGray.cgColor
        checkBox.layer.cornerRadius = 2
        
    }
    
    func setupTable(items: [Mo3tameer]) {
        self.mo3terren.append(contentsOf: items)
        for i in mo3terren{
            i.isSelected = isAllSeleced
        }
        self.tableView.reloadData()
    }

    func getMo3tamreenWithoutUi(id:Int){
        
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        
        Alamofire.request(APIs.sharedInstance.getMo3tamereen(id: id) , method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode([Mo3tameer].self, from: response.data!)
                        if temp.count == 0 {
                            self.shouldGetMore = false
                            self.activityIndicator.stopAnimating()
                        }
                        self.setupTable(items: temp)
                        self.tableView.reloadData()
                        self.pageCounter += 1
                    }catch{
                        var ftemp = "No avaiable flights with this date"
                        if Language.sharedInstance.getLang(){
                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        }
                        HUD.flash(.label(ftemp), delay: 1.0)
                        self.tableView.reloadData()
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    func postMo3tmeer(items:[Mo3tameer]) {
        var par =  ["ArnId":arrivaltemp.ARN_ID ?? 0]
        for i in 0..<items.count{
           par.updateValue(items[i].BAM_MUTAMERID ?? 0, forKey: "MutamerId[\(i)]")
            
        }
        
        if par.isEmpty{
            HUD.flash(.label(NSLocalizedString("Please Select Mutamer", comment: "من فضلك اختر معتمر")))
            return
        }
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        print(par)
        HUD.show(.progress, onView: self.view)
        let url = (arrivaltemp.TRANS_TYPE == "ARRIVAL") ?APIs.sharedInstance.confirmArriavl() : APIs.sharedInstance.confirmDeparture()
        
        
        
        print(url)
        
        Alamofire.request(url , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }

}

extension Mo3tamereenVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mo3terren.count
    }
//    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print(pageCounter)
//        
//        if self.mo3terren.count > 9 {
//        if (indexPath.row == self.mo3terren.count - 1) && shouldGetMore{
//            activityIndicator.startAnimating()
//            getMo3tamreenWithoutUi(id:arrivaltemp.ARN_ID ?? -1)
//        }
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("Mo3tamereenCell", owner: self, options: nil)?.first as! Mo3tamereenCell
        cell.SetupUI(item: mo3terren[indexPath.row], isSelectable: (arrivaltemp.TRANS_TYPE == nil) ? true : false)
        cell.selectPressed = { (tempBool) in
            
            self.mo3terren[indexPath.row].isSelected = tempBool
            
        }
        return cell
    }
}
