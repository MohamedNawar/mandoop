//
//  ProfileVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import MOLH

class ProfileVC: UIViewController {
    
    var representative = Representative()
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    @IBOutlet weak var passOld: UITextField!
    
    @IBOutlet weak var passNew: UITextField!
    
    @IBOutlet weak var passView: UIView!
    
    @IBOutlet weak var editBtnOutlet: UIButton!
    
    @IBAction func changePass(_ sender: Any) {
        PostProfileInfo()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    @IBOutlet weak var modifiyOutlet: UIButton!
    @IBAction func Modifiy(_ sender: UIButton) {
        if sender.tag == 0{
            passView.isHidden = false
            sender.tag = 1
        }else{
            passView.isHidden = true
            sender.tag = 0
        }
    }
    
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var GenderLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var passoldLbl: UILabel!
    @IBOutlet weak var passnewLbl: UILabel!
    
    
    @IBOutlet weak var save: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileInfo()
        self.title = NSLocalizedString("Profile", comment: "الملف الشخصي")
    self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "background_2"), for: .default)
        
        
        self.cityLbl.text = NSLocalizedString("City", comment: "المدينة")
        self.GenderLbl.text = NSLocalizedString("Gender", comment: "النوع")
        self.phoneLbl.text = NSLocalizedString("Phone Number", comment: "رقم الهاتف")
        self.passoldLbl.text = NSLocalizedString("Old Password", comment: "كلمة المرور القديمة")
        self.passnewLbl.text = NSLocalizedString("New Password", comment: "كلمة المرور الجديدة")
        
        self.name.placeholder = NSLocalizedString("City", comment: "المدينة")
        self.email.placeholder = NSLocalizedString("Gender", comment: "النوع")
        self.phone.placeholder = NSLocalizedString("Phone Number", comment: "رقم الهاتف")
        self.passOld.placeholder = NSLocalizedString("Old Password", comment: "كلمة المرور القديمة")
        self.passNew.placeholder = NSLocalizedString("New Password", comment: "كلمة المرور الجديدة")
        
    self.editBtnOutlet.setTitle(NSLocalizedString("Edit", comment: "تعديل"), for: .normal)
    self.save.setTitle(NSLocalizedString("Save", comment: "حفظ"), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .default)
    }
    
    func PostProfileInfo(){
        
        if self.passOld.text == "" || self.passNew.text == ""{
            HUD.flash(.labeledError(title: "حقل فارغ", subtitle: ""), delay: 1.0)
            return
        }
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        let par = [
            "OldPassword" : self.passOld.text! ,
            "NewPassword" : self.passNew.text!
        ]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.ChangePass(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    HUD.flash(.success, delay: 1.0)
                    self.passOld.text = ""
                    self.passNew.text = ""
                    self.passView.isHidden = true
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }

    func getProfileInfo(){
        
        
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.ProfileInfo(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        self.representative = try JSONDecoder().decode(Representative.self, from: response.data!)
                        self.name.text = Language.sharedInstance.getLang() ? self.representative.CT_NAME_AR : self.representative.CT_NAME_LA;
                        self.email.text = "\(self.representative.URP_IDENTIFIER_ID)"
                        self.phone.text = "\(self.representative.URP_MOBILE ?? 0)"
                        
                        
                        
                    }catch{
                        var ftemp = "No avaiable flights with this date"
                        if Language.sharedInstance.getLang(){
                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        }
                        HUD.flash(.label(ftemp), delay: 1.0)
                        
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    private func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    

}
