//
//  ProfileVC2.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/14/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import MOLH

class ProfileVC2: UIViewController {
    
    var representative = Representative()
    
    @IBOutlet weak var id: UITextField!
    @IBOutlet var userName: UILabel!
    
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    @IBOutlet weak var passOld: UITextField!
    
    @IBOutlet weak var passNew: UITextField!
    @IBOutlet weak var passNew1: UITextField!
    
    @IBOutlet weak var passView: UIView!
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBAction func changePass(_ sender: Any) {
        
            passView.isHidden = false
        modifiyOutlet.isHidden = false
        editBtn.isHidden = true

    }
    
    @IBAction func showSecondNewPass(_ sender: UIButton) {
        if sender.tag == 0 {
            passNew1.isSecureTextEntry = false
            sender.isSelected = true
            sender.tag = 1
        }else{
            passNew1.isSecureTextEntry = true
            sender.isSelected = false
            sender.tag = 0
        }
    }
    @IBAction func showFirstNewPass(_ sender: UIButton) {
        if sender.tag == 0 {
            passNew.isSecureTextEntry = false
            sender.isSelected = true
            sender.tag = 1
        }else{
            passNew.isSecureTextEntry = true
            sender.isSelected = false
            sender.tag = 0
        }
    }
    @IBAction func showOldPass(_ sender: UIButton) {
        if sender.tag == 0 {
            passOld.isSecureTextEntry = false
            sender.isSelected = true
            sender.tag = 1
        }else{
            passOld.isSecureTextEntry = true
            sender.isSelected = false
            sender.tag = 0
        }
        

    }
    @IBOutlet weak var modifiyOutlet: UIButton!
    @IBAction func Modifiy(_ sender: UIButton) {
        PostProfileInfo()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileInfo()
       
        self.title = NSLocalizedString("Profile", comment: "الملف الشخصي")
    self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "background_2"), for: .default)
        self.passOld.placeholder = NSLocalizedString("Old Password", comment: "كلمة المرور القديمة")
        self.passNew.placeholder = NSLocalizedString("New Password", comment: "كلمة المرور الجديدة")
        self.passNew1.placeholder = NSLocalizedString("New Password", comment:"كلمة المرور الجديدة")
        self.city.placeholder = NSLocalizedString("City", comment: "المدينة")
        self.id.placeholder = NSLocalizedString("Identity No.", comment: "رقم العضوية")
        self.phone.placeholder = NSLocalizedString("Phone Number", comment: "رقم الهاتف")
    self.editBtn.setTitle(NSLocalizedString("Edit", comment: "تعديل"), for: .normal)
    self.modifiyOutlet.setTitle(NSLocalizedString("Save", comment: "حفظ"), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .default)
    }
    
    func PostProfileInfo(){
        
        if self.passOld.text == "" || self.passNew.text == "" || self.passNew1.text == "" {
            HUD.flash(.labeledError(title: "حقل فارغ", subtitle: ""), delay: 1.0)
            passView.isHidden = false
            modifiyOutlet.isHidden = false
            editBtn.isHidden = true

            return
        }
        if self.passNew.text != self.passNew1.text {
            HUD.flash(.labeledError(title: "كلمة السر غير منطابقة", subtitle: ""), delay: 1.0)
            passView.isHidden = false
            modifiyOutlet.isHidden = false
            editBtn.isHidden = true

            return
        }
        passView.isHidden = true
        modifiyOutlet.isHidden = true
        editBtn.isHidden = false
        passOld.text = ""
        passNew.text = ""
        passNew1.text = ""
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        let par = [
            "OldPassword" : self.passOld.text! ,
            "NewPassword" : self.passNew.text!
        ]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.ChangePass(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        HUD.flash(.labeledError(title: err.error_description, subtitle: err.error_description), delay: 1.0)
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    HUD.flash(.success, delay: 1.0)
                    self.passOld.text = ""
                    self.passNew.text = ""
                    self.passView.isHidden = true
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }

    func getProfileInfo(){
        
        
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.ProfileInfo(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        HUD.flash(.labeledError(title: err.error_description, subtitle: err.error_description), delay: 1.0)
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        self.representative = try JSONDecoder().decode(Representative.self, from: response.data!)
                        
                        self.city.text = Language.sharedInstance.getLang() ? self.representative.CT_NAME_AR : self.representative.CT_NAME_LA
                        self.id.text =  "\(self.representative.URP_IDENTIFIER_ID ?? 0)"
                        self.phone.text = "\(self.representative.URP_MOBILE ?? 0)"
                          self.userName.text = "\(self.representative.URP_NAME  ?? "")"
                        
                        
                    }catch{
                        var ftemp = "No avaiable flights with this date"
                        if Language.sharedInstance.getLang(){
                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        }
                        HUD.flash(.labeledError(title: "", subtitle: ftemp), delay: 1.0)
                        
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    

}
