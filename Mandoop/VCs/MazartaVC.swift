//
//  MazartaVC.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/13/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import YNDropDownMenu
import FCAlertView
import MOLH
class MazartaVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var shouldGetMore = true
    var pageCounter = 1
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var arrivalArr = [Arrival]()
    var filterBtnPreesed = false
    var dateTemp = ""
    var EATemp = "-1"
    var cityTemp = "-1"
    var countryTemp = "-1"
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Mazarat", comment: "المزارات")
        dateTemp = getDate()
    
        //        self.navigationController?.navigationBar.barTintColor = .clear
        // Do any additional setup after loading the view.
        
        pageCounter = 1
        getMazaratWithoutUi(TripDate: dateTemp, EA_CODE: EATemp, CityID: cityTemp, PageNumber: "\(pageCounter)")
        
    }
    @IBAction func filterBtn(_ sender: Any) {
        let popUpVC = storyboard?.instantiateViewController(withIdentifier: "MazaratPopUp") as! MazaratPopUp
        popUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popUpVC.delegate = self
        popUpVC.dateTemp = dateTemp
        popUpVC.AgentTemp = EATemp
        popUpVC.CityTemp = cityTemp
        popUpVC.CounteryTemp = countryTemp
        present(popUpVC, animated: true, completion: nil)
        
    }
    
//    private func setupViews(flag:Bool){
//        var ZBdropDownViews = Bundle.main.loadNibNamed("MazaratFilterView", owner: nil, options: nil) as? [UIView]
//        let x = ZBdropDownViews![0] as! MazaratFilterView
//        x.delegateTemp = self
//        ZBdropDownViews![0] = x
//        if let _ZBdropDownViews = ZBdropDownViews {
//            // Inherit YNDropDownView if you want to hideMenu in your dropDownViews
//
//            let view = YNDropDownMenu(frame: CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 0), dropDownViews: _ZBdropDownViews, dropDownViewTitles: [""])
//
//
//            // Add custom blurEffectView
//            let backgroundView = UIView()
//            backgroundView.backgroundColor = .black
//            view.blurEffectView = backgroundView
//            view.blurEffectViewAlpha = 0.7
//
//            // Open and Hide Menu
//            view.alwaysSelected(at: 0)
//
//            if flag {
//                self.view.addSubview(view)
//                view.showAndHideMenu(at: 0)
//                filterBtnPreesed = false
//            }else{
//                view.hideMenu()
//            }
//
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrivalArr.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrivalArr.count > 9 {
        if (indexPath.row == self.arrivalArr.count - 1) && shouldGetMore{
            activityIndicator.startAnimating()
            getMazaratWithoutUi(TripDate: dateTemp, EA_CODE: EATemp, CityID: cityTemp, PageNumber: "\(pageCounter)")
        }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("MazaratCell", owner: self, options: nil)?.first as! MazaratCell
        cell.UISetup(arrival: self.arrivalArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Mo3tamereenVC") as! Mo3tamereenVC
        vc.arrivaltemp = arrivalArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
//
//    func getMazarat(TripDate: String, EA_CODE: String, CityID: String, PageNumber: String){
//
//        arrivalArr.removeAll()
//
//        let header = [
//            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
//            "Accept" : "application/json"
//            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
//        ]
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.sharedInstance.getMazarat(TripDate: TripDate, EA_CODE: EA_CODE, CityID: CityID, PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//
//
//
//            switch(response.result) {
//            case .success(let value):
//                print(response.value)
//                print(response.result.error)
//                print(response.response?.statusCode)
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//
//                    do {
//                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
//                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                        //                            user.saveUser(user: user)
//                    }catch{
//                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//
//                    do {
//                        self.arrivalArr = try JSONDecoder().decode([Arrival].self, from: response.data!)
//                        self.tableView.reloadData()
//                        self.pageCounter += 1
//
//                    }catch{
//                        var ftemp = "No avaiable flights with this date"
//                        if Language.sharedInstance.getLang(){
//                            ftemp = "لا يوجد رحلات بهذا التاريخ"
//                        }
//                        HUD.flash(.label(ftemp), delay: 1.0)
//                        self.tableView.reloadData()
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    
    
    func getMazaratWithoutUi(TripDate: String, EA_CODE: String, CityID: String, PageNumber: String){
        
//        arrivalArr.removeAll()
        
        let header = [
            "Authorization" : "Bearer \(user.access_token ?? "API TOKEN")" ,
            "Accept" : "application/json"
            , "lan" : "\(MOLHLanguage.currentAppleLanguage())"
        ]
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getMazarat(TripDate: TripDate, EA_CODE: EA_CODE, CityID: CityID, PageNumber: PageNumber), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
          print(APIs.sharedInstance.getMazarat(TripDate: TripDate, EA_CODE: EA_CODE, CityID: CityID, PageNumber: PageNumber))
            
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: err.Message ?? "", SubTitle: "", Image: #imageLiteral(resourceName: "Logo_blue"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        //                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode([Arrival].self, from: response.data!)
                        if temp.count == 0 {
                            self.shouldGetMore = false
                            self.activityIndicator.stopAnimating()
                        }else{
                            self.arrivalArr.append(contentsOf: temp)
                            self.tableView.beginUpdates()
                            
                            self.tableView.insertRows(at: [IndexPath(row: self.arrivalArr.count-1, section: 0)], with: .automatic)
                            
                            self.tableView.endUpdates()
                            self.pageCounter += 1
                        }
                        
                    }catch{
                        var ftemp = "No avaiable flights with this date"
                        if Language.sharedInstance.getLang(){
                            ftemp = "لا يوجد رحلات بهذا التاريخ"
                        }
                        HUD.flash(.label(ftemp), delay: 1.0)
                        self.tableView.reloadData()
                    }
                }
            case .failure(_):
//                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    private func getDate() -> String{//return date as formated -> year-day-month 11-15-2017
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        return "\(month)-\(day)-\(year)"
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
    let alert = FCAlertView()
    alert.avoidCustomImageTint = true
    alert.colorScheme = ColorControl.sharedInstance.getMainColor()
    alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
    
}

extension MazartaVC : MazaratPopUpDelegte{
    func popUpValues(TripDate: String, EA_CODE: String, CityID: String, PageNumber: String, country : String) {
        filterBtnPreesed = false
        pageCounter = 1
        shouldGetMore = true
        dateTemp = TripDate
        EATemp = EA_CODE
        cityTemp = CityID
        countryTemp = country
        getMazaratWithoutUi(TripDate: dateTemp, EA_CODE: EA_CODE, CityID: CityID, PageNumber: "\(pageCounter)")
    }
    
    func justBeingCancelled() {
        filterBtnPreesed = false
    }
    
    
}
