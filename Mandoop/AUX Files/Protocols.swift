//
//  Protocols.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/11/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import Foundation

protocol ArrivalsPopUpDelegte {
    func popUpValues(flightType: String, EA_CODE: String, TripDate: String, CityID: String, ReqID: String, AIRLINE_NO: String, PageNumber: String, country : String)
    func justBeingCancelled()
}

protocol MazaratPopUpDelegte {
    func popUpValues(TripDate: String, EA_CODE: String, CityID: String, PageNumber: String, country : String)
    func justBeingCancelled()
}
protocol MazaratPopUpDelegte1 {
    func popUpValues(TripDate: String, EA_CODE: String, CityID: String, PageNumber: String, country : String)
    func justBeingCancelled()
}
protocol FlightPopUpDelegte {
    func popUpValues(flightType: String, AirlineCode: String, TripDate: String, FlightNO: String, TripStatus: String, TripCountry: String, TripCity: String, PageNumber: String)
    func justBeingCancelled()
}
