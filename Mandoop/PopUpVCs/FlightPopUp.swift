//
//  FlightPopUp.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/20/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import McPicker
import ActionSheetPicker_3_0
import PKHUD
import MOLH
class FlightPopUp: UIViewController {
    
    var delegate: FlightPopUpDelegte?
    var typeTemp = "-1"
    var dateTemp = ""
    var CityTemp = "-1"
    var CountryTemp = "-1"
    var statusTemp = "-1"
    var FlightComTemp = "-1"
    var FlightNO = "-1"
    
    
    
    
    @IBOutlet weak var FlightType1: UILabel!
    @IBOutlet weak var date1: UILabel!
    @IBOutlet weak var countery1: UILabel!
    @IBOutlet weak var city1: UILabel!
    @IBOutlet weak var stats1: UILabel!
    @IBOutlet weak var aircode1: UILabel!
    @IBOutlet weak var flightComp1: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    
    
    @IBOutlet weak var flightTypeView: UIView!
    @IBOutlet weak var flightTypeLbl: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLbl: UITextField!
    
    @IBOutlet weak var CountryView: UIView!
    @IBOutlet weak var CountryLbl: UITextField!
    
    @IBOutlet weak var CityView: UIView!
    @IBOutlet weak var CityLbl: UITextField!
    
    
    
    @IBOutlet weak var airlineCodeView: UIView!
    
    @IBOutlet weak var airlineCodeLbl: UITextField!
    @IBOutlet weak var StatusView: UIView!
    @IBOutlet weak var StatusLbl: UITextField!
    @IBOutlet weak var flightNumLbl: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if MOLHLanguage.currentAppleLanguage() == "en"{
            flightTypeLbl.addPadding(UITextField.PaddingSide.left(20))
            StatusLbl.addPadding(UITextField.PaddingSide.left(20))
            airlineCodeLbl.addPadding(UITextField.PaddingSide.left(20))
            CountryLbl.addPadding(UITextField.PaddingSide.left(20))
            CityLbl.addPadding(UITextField.PaddingSide.left(20))
            flightNumLbl.addPadding(UITextField.PaddingSide.left(20))
            dateLbl.addPadding(UITextField.PaddingSide.left(20))
            flightTypeLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            StatusLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            airlineCodeLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            
            CountryLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            CityLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            dateLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            
            
        }else{
            flightTypeLbl.addPadding(UITextField.PaddingSide.right(20))
            StatusLbl.addPadding(UITextField.PaddingSide.right(20))
            airlineCodeLbl.addPadding(UITextField.PaddingSide.right(20))
            CountryLbl.addPadding(UITextField.PaddingSide.right(20))
            CityLbl.addPadding(UITextField.PaddingSide.right(20))
            flightNumLbl.addPadding(UITextField.PaddingSide.right(20))
            dateLbl.addPadding(UITextField.PaddingSide.right(20))
            flightTypeLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            StatusLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            airlineCodeLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            CountryLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            CityLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
            dateLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        }
        
     
        let date = Date()
        dateTemp = getDate(date: date)
        self.initViews()
        self.setuplang()
        self.setupView()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissSelf(_:))))
    }
    @objc private func dismissSelf(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        
        
        
        if flightNumLbl.text != ""{
            FlightNO = flightNumLbl.text ?? "-1"
        }
        
        
        delegate?.popUpValues(flightType: typeTemp, AirlineCode: FlightComTemp , TripDate: dateTemp, FlightNO: FlightNO, TripStatus: statusTemp, TripCountry: CountryTemp, TripCity: CityTemp, PageNumber: "1")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegate?.justBeingCancelled()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func initViews() {
        Countries.sharedInstance.getCountriesServer()
        AirLines.sharedInstance.getAirlinesServer()
        //        Agents.sharedInstance.getAgentsServer()
        flightTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightTypePressed(_:))))
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        CityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        CountryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.counteryPressed(_:))))
        StatusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.statusPressed(_:))))
        
        airlineCodeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightComPressed(_:))))
    }
    
    private func setupView(){
        switch self.typeTemp{
        case "1":
            self.flightTypeLbl.text = NSLocalizedString("Arrival", comment: "وصول")
        case "2":
            self.flightTypeLbl.text = NSLocalizedString("Departure", comment: "سفر")
        default:
            self.flightTypeLbl.text = ""
        }
        self.dateLbl.text = dateTemp
        self.CityLbl.text = Cities.sharedInstance.getCityName(id: Int(CityTemp) ?? -1, lang: Language.sharedInstance.getLang())
        self.airlineCodeLbl.text = AirLines.sharedInstance.getAirlineName(id: Int(FlightComTemp) ?? -1, lang: Language.sharedInstance.getLang())
//        self.agentLbl.text = Agents.sharedInstance.getAgentName(code: Int(AgentTemp) ?? -1, lang: Language.sharedInstance.getLang())
        
        //        self.EALbl.text = (EATemp == "-1") ? "" : EATemp
        self.flightNumLbl.text = (FlightNO == "-1") ? "" : FlightNO
        
        
    }
    
    @objc private func flightComPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = AirLines.sharedInstance.getAirlinesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.airlineCodeLbl.text = StringTemp
                    self.FlightComTemp = AirLines.sharedInstance.getAirLinesId(name: StringTemp)
                }
            }
        }
    }
    @objc private func flightTypePressed(_ sender: UITapGestureRecognizer) {
        var ftemp = ["Arrival","Departure","All"]
        if Language.sharedInstance.getLang(){
            ftemp = ["وصول","سفر","الكل"]
        }
        let mcPicker = McPicker(data: [ftemp])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
                self.flightTypeLbl.text = StringTemp
                switch StringTemp{
                case "Arrival":
                    self.typeTemp = "1"
                case "وصول":
                    self.typeTemp = "1"
                case "Departure":
                    self.typeTemp = "2"
                case "سفر":
                    self.typeTemp = "2"
                default:
                    self.typeTemp = "-1"
                }
            }
        }
    }
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in
            
            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self.view)
        
    }
    
    @objc private func counteryPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Countries.sharedInstance.getCountriesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.CountryLbl.text = StringTemp
                    self.CountryTemp = Countries.sharedInstance.getCountriesId(name: StringTemp)
                    Cities.sharedInstance.getCitiesServer(id: Int(self.CountryTemp) ?? -1)
                }
            }
        }
    }
    
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Cities.sharedInstance.getCitiesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.CityLbl.text = StringTemp
                    self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
                }
            }
        }else{
            if Language.sharedInstance.getLang(){
                HUD.flash(.label("يجب عليك اختيار البلد أولا"), delay: 1.0)
            }else{
                HUD.flash(.label("You have to choose Countery First"), delay: 1.0)
            }
            
        }
    }
    
    @objc private func statusPressed(_ sender: UITapGestureRecognizer) {
        var mcPicker = McPicker()
        if MOLHLanguage.currentAppleLanguage() == "en" {
             mcPicker = McPicker(data: [["Active","Not Active"]])
        }else{
           mcPicker = McPicker(data: [["مفعل","غير مفعل"]])
        }
        mcPicker.show {  (selections: [Int : String]) -> Void in
            
            if let StringTemp = selections[0] {
                self.StatusLbl.text = StringTemp
                if MOLHLanguage.currentAppleLanguage() == "en" {
                    if StringTemp == "مفعل" {
                        self.statusTemp = "0"
                    }else{
                        self.statusTemp = "512"
                    }
                }else{
                    if StringTemp == "Active"{
                        self.statusTemp = "0"
                    }else{
                        self.statusTemp = "512"
                    }                }
            }
        }
    }
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
       return "\(day)-\(month)-\(year)"
    }
    
    private func setuplang(){
        FlightType1.text = NSLocalizedString("Flight Type :", comment: "نوع الرحلة :")
        date1.text = NSLocalizedString("Date :", comment: "التاريخ :")
        countery1.text = NSLocalizedString("Countery :", comment: "الدولة :")
        city1.text = NSLocalizedString("City :", comment: "المدينة :")
        stats1.text = NSLocalizedString("Status :", comment: "الحالة :")
        flightComp1.text = NSLocalizedString("Flight Compnay :", comment: " شركة الطيران :")
        aircode1.text = NSLocalizedString("Flight Num :", comment: "رقم الرحلة :")
        cancelBtn.setTitle(NSLocalizedString("Cancel", comment: "إلغاء"), for: .normal)
        okBtn.setTitle(NSLocalizedString("OK", comment: "ابحث"), for: .normal)
        
    }
    
    
}
