//
//  ArrivalAndDeparturePopUp.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/19/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import McPicker
import ActionSheetPicker_3_0
import PKHUD
import MOLH
class ArrivalAndDeparturePopUp: UIViewController {


    
    override func viewDidLoad() {
        super.viewDidLoad()
  
       if MOLHLanguage.currentAppleLanguage() == "en"{
        flightNumLbl.addPadding(UITextField.PaddingSide.left(20))
        agentLbl.addPadding(UITextField.PaddingSide.left(20))
        flightComLbl.addPadding(UITextField.PaddingSide.left(20))
        flightTypeLbl.addPadding(UITextField.PaddingSide.left(20))
        dateLbl.addPadding(UITextField.PaddingSide.left(20))
        cityLbl.addPadding(UITextField.PaddingSide.left(20))
        agentLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        flightComLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        flightTypeLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        dateLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityLbl.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        
       }else{
        flightNumLbl.addPadding(UITextField.PaddingSide.right(20))
        agentLbl.addPadding(UITextField.PaddingSide.right(20))
        flightComLbl.addPadding(UITextField.PaddingSide.right(20))
        flightTypeLbl.addPadding(UITextField.PaddingSide.right(20))
        dateLbl.addPadding(UITextField.PaddingSide.right(20))
        cityLbl.addPadding(UITextField.PaddingSide.right(20))
        agentLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        flightComLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        flightTypeLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        dateLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityLbl.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        }
        
//        let date = Date()
//        if da
//        dateTemp = getDate(date: date)
        self.initViews()
        setuplang()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissSelf(_:))))
        setupView()
        
    }
    
    private func setupView(){
        switch self.typeTemp{
        case "1":
             self.flightTypeLbl.text = NSLocalizedString("Arrival", comment: "وصول")
        case "2":
            self.flightTypeLbl.text = NSLocalizedString("Departure", comment: "سفر")
        default:
            self.flightTypeLbl.text = ""
        }
        self.dateLbl.text = dateTemp
        self.cityLbl.text = Cities.sharedInstance.getCityName(id: Int(CityTemp) ?? -1, lang: Language.sharedInstance.getLang())
        self.flightComLbl.text = AirLines.sharedInstance.getAirlineName(id: Int(FlightComTemp) ?? -1, lang: Language.sharedInstance.getLang())
        self.agentLbl.text = Agents.sharedInstance.getAgentName(code: Int(AgentTemp) ?? -1, lang: Language.sharedInstance.getLang())
        
//        self.EALbl.text = (EATemp == "-1") ? "" : EATemp
        self.flightNumLbl.text = (flightNumTemp == "-1") ? "" : flightNumTemp
        
       
    }
    @objc private func dismissSelf(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    var delegate: ArrivalsPopUpDelegte?
    var typeTemp = "-1"
    var dateTemp = "-1"
    var CityTemp = "-1"
    var FlightComTemp = "-1"
    var AgentTemp = "-1"
    var EATemp = "-1"
    var flightNumTemp = "-1"
    
    
    @IBOutlet weak var FlightType1: UILabel!
    @IBOutlet weak var date1: UILabel!
    @IBOutlet weak var flightCompany: UILabel!
    @IBOutlet weak var city1: UILabel!
    @IBOutlet weak var agent1: UILabel!
//    @IBOutlet weak var ea1: UILabel!
    @IBOutlet weak var flight1: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    
    
    @IBOutlet weak var flightTypeView: UIView!
    @IBOutlet weak var flightTypeLbl: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLbl: UITextField!
    
    @IBOutlet weak var fleightView: UIView!
    @IBOutlet weak var flightComLbl: UITextField!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLbl: UITextField!
    
    @IBOutlet weak var agentView: UIView!
    @IBOutlet weak var agentLbl: UITextField!
    
    
//    @IBOutlet weak var EALbl: UITextField!
    
    @IBOutlet weak var flightNumLbl: UITextField!
    
   
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        
        if flightNumLbl.text != ""{
            flightNumTemp = flightNumLbl.text ?? "-1"
        }
        delegate?.popUpValues(flightType: typeTemp, EA_CODE: EATemp , TripDate: dateTemp, CityID: CityTemp, ReqID: AgentTemp, AIRLINE_NO: flightNumTemp, PageNumber: "1", country: FlightComTemp)
        initViews()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        delegate!.justBeingCancelled()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    func initViews() {
        Cities.sharedInstance.getCitiesForUser()
        Agents.sharedInstance.getAgentsServer()
        AirLines.sharedInstance.getAirlinesServer()
        flightTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightTypePressed(_:))))
        dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.datePressed(_:))))
        fleightView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.flightComPressed(_:))))
        cityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cityPressed(_:))))
        agentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.agentPressed(_:))))
        
    }
    
    @objc private func flightTypePressed(_ sender: UITapGestureRecognizer) {
        var ftemp = ["Arrival","Departure","All"]
        if Language.sharedInstance.getLang(){
            ftemp = ["وصول","سفر","الكل"]

        }
        let mcPicker = McPicker(data: [ftemp])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
                self.flightTypeLbl.text = StringTemp
                switch StringTemp{
                case "Arrival":
                    self.typeTemp = "1"
                case "وصول":
                    self.typeTemp = "1"
                case "Departure":
                    self.typeTemp = "2"
                case "سفر":
                    self.typeTemp = "2"
                default:
                    self.typeTemp = "-1"
                }
            }
        }
    }
    
    @objc private func datePressed(_ sender: UITapGestureRecognizer) {
        let date = Date()
        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: date, doneBlock: {
            picker, indexes, values in
            
            
            if let dateTemp2 = indexes {
                self.dateTemp = self.getDate(date: dateTemp2 as! Date)
                self.dateLbl.text = self.getDate(date: dateTemp2 as! Date)
            }
            return
        }, cancel: nil, origin: self.view)
        
    }
    
    @objc private func flightComPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = AirLines.sharedInstance.getAirlinesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
            let mcPicker = McPicker(data: [cotemp])
            mcPicker.show {  (selections: [Int : String]) -> Void in
                if let StringTemp = selections[0] {
                    self.flightComLbl.text = StringTemp
                    self.FlightComTemp = AirLines.sharedInstance.getAirLinesId(name: StringTemp)
                }
            }
        }
    }
    @objc private func cityPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Cities.sharedInstance.getCitiesNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
        let mcPicker = McPicker(data: [cotemp])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0] {
                self.cityLbl.text = StringTemp
                self.CityTemp = Cities.sharedInstance.getCitiesId(name: StringTemp)
            }
        }
        }
        
    }
    
    @objc private func agentPressed(_ sender: UITapGestureRecognizer) {
        let cotemp = Agents.sharedInstance.getAgentsNameArr(lang: Language.sharedInstance.getLang())
        if !cotemp.isEmpty{
        let mcPicker = McPicker(data: [cotemp])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            
            if let StringTemp = selections[0] {
                self.agentLbl.text = StringTemp
                self.AgentTemp = Agents.sharedInstance.getAgentsAgentCode(name: StringTemp)
            }
            
            }
        }
    }
    
    private func getDate(date:Date) -> String{//return date as formated -> year-day-month 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
          return "\(day)-\(month)-\(year)"
    }
    
    
    private func setuplang(){
        FlightType1.text = NSLocalizedString("Flight Type :", comment: "نوع الرحلة :")
        date1.text = NSLocalizedString("Date :", comment: "الميعاد :")
        flightCompany.text = NSLocalizedString("Flight company :", comment: "شركة الطيران :")
        city1.text = NSLocalizedString("City :", comment: "المدينة :")
        agent1.text = NSLocalizedString("Agent :", comment: "الوكالة :")
//        ea1.text = NSLocalizedString("EA :", comment: "الوكيل :")
        flight1.text = NSLocalizedString("Flight Num :", comment: "رقم الرحلة :")
        cancelBtn.setTitle(NSLocalizedString("Cancel", comment: "إلغاء"), for: .normal)
        okBtn.setTitle(NSLocalizedString("OK", comment: "ابحث"), for: .normal)
        
    }

}
