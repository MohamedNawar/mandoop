//
//  AppDelegate.swift
//  Mandoop
//
//  Created by Moaz Ezz on 6/7/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import SideMenu
import IQKeyboardManagerSwift
import MOLH
import OneSignal
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSSubscriptionObserver {
    
    

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        MOLH.shared.activate(true)
//        MOLH.shared.specialKeyWords = ["Cancel","Done"]
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.window!)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.window!)
        
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPushStyle = .popWhenPossible
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        let ToolBarAppearace = UIToolbar.appearance()
        let BarAppearace = UINavigationBar.appearance()
        let imageTemp = #imageLiteral(resourceName: "navbar")
        BarAppearace.setBackgroundImage(imageTemp, for: .default)
        
        BarAppearace.backgroundColor = .clear
        BarAppearace.titleTextAttributes = [
            NSAttributedStringKey.font: ColorControl.sharedInstance.getNormalFont(size: 12)!,
            NSAttributedStringKey.foregroundColor: ColorControl.sharedInstance.getSecoundColor()
        ]
        ToolBarAppearace.backgroundColor = ColorControl.sharedInstance.getMainColor()
        ToolBarAppearace.barTintColor = ColorControl.sharedInstance.getMainColor()
        ToolBarAppearace.tintColor = ColorControl.sharedInstance.getSecoundColor()
        let ButtonBarAppearace = UIBarButtonItem.appearance()
        ButtonBarAppearace.setTitleTextAttributes([
            NSAttributedStringKey.font: ColorControl.sharedInstance.getNormalFont(size: 17) as Any
            ], for: UIControlState.normal)
        ButtonBarAppearace.tintColor = UIColor.white
        
        IQKeyboardManager.shared.enable = true
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "bb432415-0bb9-4710-96f3-3d412984ce22",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)

        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "rootnav")
    }

    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
            print(UserDefaults.standard.string(forKey: "onesignalid"))
            print("Current playerId \(playerId)")
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addOneSignalIdNC"), object: nil, userInfo: nil)
            
        }
    }
}

